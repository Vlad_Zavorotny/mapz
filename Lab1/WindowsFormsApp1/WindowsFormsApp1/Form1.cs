﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void onClick(object sender, EventArgs e)
        {
            textBox2.Clear();
            int count = treeView1.Nodes.Count;
            for(int a=0;a<count;a++)
            {
                treeView1.Nodes.RemoveAt(0);
            }
            int count2 = treeView2.Nodes.Count;
            for (int a = 0; a < count2; a++)
            {
                treeView2.Nodes.RemoveAt(0);
            }
            Lexer lexer=new Lexer();
            Parser parser = new Parser();
            Context context = new Context();
            string text = textBox1.Text;
            List<ExpressionBase> lexp=lexer.Execute(text, context);
            parser.Execute(ref context, ref lexp);
            textBox2.Text = context.output;
            lexp = lexer.Execute(text, context);
            List<ExpressionBase> treeList = new List<ExpressionBase>();
            while (lexp.Count != 0)
            {
                ExpressionBase tempTree = parser.CreateTree(ref lexp);
                treeList.Add(tempTree);
            }
            int i = 0;
            foreach(ExpressionBase node in treeList)
            {
                TreeNode temp = new TreeNode();
                treeView1.Nodes.Add(temp);
                insertNode(treeView1.Nodes[i], node);
                i++;
            }
            treeList = new List<ExpressionBase>();
            lexp = lexer.Execute(text,context);
            while (lexp.Count != 0)
            {
                ExpressionBase tempTree = parser.CreateTree(ref lexp);
                treeList.Add(tempTree);
            }
            treeList = parser.Optimization(treeList,ref context);
            i = 0;
            foreach (ExpressionBase node in treeList)
            {
                TreeNode temp = new TreeNode();
                treeView2.Nodes.Add(temp);
                insertNode(treeView2.Nodes[i], node);
                i++;
            }
        }
        

        private TreeNode insertNode(TreeNode root,ExpressionBase node)
        {
            if(node!=null)
            root.Text=node.text;
            
            switch (node)
            {
                case OpenBraceExpression exlist:
                    TreeNode temp = new TreeNode();
                    TreeNode[] tlist = new TreeNode[exlist.OperandList.Count];
                    for(int i=0;i<exlist.OperandList.Count;i++)
                    {
                        temp = new TreeNode();
                        temp = insertNode(temp, exlist.OperandList.ElementAt(i));
                        tlist[i] = (TreeNode)temp.Clone();
                        root.Nodes.Add(tlist[i]);
                    }
                    break;
                case IfElseExpression ex3:
                    TreeNode op1 = new TreeNode();
                    root.Nodes.Add(insertNode(op1, ex3.Operand1));
                    TreeNode op2 = new TreeNode();
                    root.Nodes.Add(insertNode(op2, ex3.Operand2));
                    TreeNode op3 = new TreeNode();
                    root.Nodes.Add(insertNode(op3, ex3.Operand3));
                    break;
                case BinaryExpression ex2:
                    TreeNode op4 = new TreeNode();
                    root.Nodes.Add(insertNode(op4, ex2.Operand1));
                    TreeNode op5 = new TreeNode();
                    root.Nodes.Add(insertNode(op5, ex2.Operand2));
                    break;
                case UnaryExpression ex1:
                    TreeNode op6=new TreeNode();
                    root.Nodes.Add(insertNode(op6, ex1.Operand1));
                    break;
            }
            return root;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }

}
