﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Lexer
    {
        private ExpressionBase GetToken(ref string text)
        {
            ExpressionBase expression = null;
            string reString ="\".*";
            Regex re = new Regex(reString);
            text = text.Trim(' ');
            var firstToken = text.Split(' ','\n')[0].Replace("\r","");
      
            if (firstToken == "var")
            {
                expression = new VarExpression();
            }
            else if (firstToken == "=")
            {
                expression = new AssignmentExpression();
            }
            else if (firstToken == "+")
            {
                expression = new PlusExpression();
            }
            else if (firstToken == "-")
            {
                expression = new MinusExpression();
            }
            else if (firstToken == "*")
            {
                expression = new MultiplyExpression();
            }
            else if (firstToken == "/")
            {
                expression = new DivisionExpression();
            }
            else if (firstToken == "print")
            {
                expression = new PrintExpression();
            }
            else if (firstToken == "letter")
            {
                expression = new LetterExpression();
            }
            else if (firstToken == "is_in")
            {
                expression = new IsInExpression();
            }
            else if (firstToken == "count_in")
            {
                expression = new CountInExpression();
            }
            else if (firstToken == "if")
            {
                expression = new IfElseExpression();
            }
            else if (firstToken=="else")
            {
                expression = new ElseExpression();
            }
            else if (firstToken == "while")
            {
                expression = new WhileExpression();
            }
            else if (firstToken == "match")
            {
                expression = new MatchExpression();
            }
            else if (firstToken == "|")
            {
                expression = new ConcatExpression();
            }
            else if (firstToken == ">")
            {
                expression = new GreaterExpression();
            }
            else if (firstToken == "<")
            {
                expression = new LesserExpression();
            }
            else if (firstToken == "==")
            {
                expression = new EqualExpression();
            }
            else if (firstToken == "!=")
            {
                expression = new NotEqualExpression();
            }
            else if (firstToken == ">=")
            {
                expression = new GreaterEqualExpression();
            }
            else if (firstToken == "<=")
            {
                expression = new LesserEqualExpression();
            }
            else if (firstToken == "!")
            {
                expression = new NotExpression();
            }
            else if (firstToken == "&&")
            {
                expression = new AndExpression();
            }
            else if (firstToken == "||")
            {
                expression = new OrExpression();
            }
            else if (firstToken == ";")
            {
                expression = new SemicolonExpression();
            }
            else if (firstToken == "(")
            {
                expression = new OpenBracketExpression();
            }
            else if (firstToken == ")")
            {
                expression = new CloseBracketExpression();
            }
            else if (firstToken == "{")
            {
                expression = new OpenBraceExpression();
            }
            else if (firstToken == "}")
            {
                expression = new CloseBraceExpression();
            }
            else if (firstToken.All(_ => char.IsDigit(_)))
            {
                expression = new ConstantExpression(firstToken);
            }
            else if (firstToken.All(_ => char.IsLetterOrDigit(_)))
            {
                expression = new VariableExpression(firstToken);
            }
            else if (re.IsMatch(firstToken))//re.IsMatch(firstToken)
            {
                StringBuilder sb = new StringBuilder();
                text=text.Remove(0, 1);
                char currentChar = text[0];
                while (currentChar != '"')
                {
                    sb.Append(currentChar);
                    text=text.Remove(0, 1);
                    currentChar = text[0];
                }
                text = text.Remove(0, 1);
                expression = new ConstantExpression(sb.ToString());
                return expression;
            }
            else
            {
                throw new Exception($"Unknow expression {firstToken};");
            }

            text = text.Remove(0, firstToken.Length).Trim('\r', ' ','\n','\t');
            return expression;
        }
        public List<ExpressionBase> Execute(string text, Context context)
        {
                List<ExpressionBase> tokens = new List<ExpressionBase>();
                

                string expression = text;
                SetSpaces(ref expression);
                do
                {
                    ExpressionBase token = GetToken(ref expression);
                    tokens.Add(token);
                    if (token.type == ExpressionType.IFELSE)
                    {
                        ExpressionBase temp = new IfExpression();
                        tokens.Add(temp);
                    }
                }
                while (!string.IsNullOrWhiteSpace(expression));
            return tokens;
        }
        private void SetSpaces(ref string text)
        {
            for (int i = 0; i < text.Length; i++)
            {
                char current = text[i];
                if(current == '"')
                {
                    if (i > 0)
                    {
                        if (text[i - 1] != ' ')
                        {
                            text = text.Insert(i, " ");
                            i++;
                        }
                    }
                    i++;
                    current = text[i];
                    while (current != '"')
                    {
                        i++;
                        current = text[i];
                    }
                    if (i != text.Length - 1)
                    {
                        if (text[i + 1] != ' ')
                        {
                            text = text.Insert(i + 1, " ");
                        }
                    }


                }
                else if(current == '(' || current == ')' || current == '{' || current == '}'||current==';'||current=='+'||current=='-'||current=='*'||current=='/')
                {
                    if (i != text.Length-1)
                    {
                        if(text[i+1]!=' ')
                        {
                            text = text.Insert(i + 1, " ");
                        }   
                    }
                    if (i > 0)
                    {
                        if(text[i-1]!=' ')
                        {
                            text = text.Insert(i, " ");
                            i++;
                        }
                    }
                }else if(current == '=')
                {
                    if(!(text[i-1]=='>'||text[i-1]=='<'||text[i-1]=='='||text[i-1]=='!'||text[i-1]==' '))
                    {
                        text = text.Insert(i, " ");
                        i++;
                    }
                    if (text[i + 1] != '='&& text[i+1]!=' ')
                    {
                        text = text.Insert(i + 1, " ");
                    }
                }else if(current == '>'||current=='<'||current=='!')
                {
                    if (text[i - 1] != ' ')
                    {
                        text = text.Insert(i, " ");
                        i++;
                    }
                    if (text[i + 1] != '='&&text[i+1]!=' ')
                    {
                        text = text.Insert(i + 1, " ");
                    }
                }else if(current == '&')
                {
                    if(text[i-1]!='&'&&text[i-1]!=' ')
                    {
                        text = text.Insert(i, " ");
                        i++;
                    }
                    if(text[i+1]!='&'&&text[i+1]!=' ')
                    {
                        text = text.Insert(i + 1, " ");
                    }
                }
                else if (current == '|')
                {
                    if (text[i - 1] != '|' && text[i - 1] != ' ')
                    {
                        text = text.Insert(i, " ");
                        i++;
                    }
                    if (text[i + 1] != '|' && text[i + 1] != ' ')
                    {
                        text = text.Insert(i + 1, " ");
                    }
                }

            }
        }
        
    }

}
