﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    enum ExpressionType {
        VARIABLE,
        CONST,
        ASSIGNMENT,
        PLUS,
        MINUS,
        MULTIPLY,
        DEVIDE,
        PRINT,
        VAR,
        LETTER,
        CONCAT,
        ISIN,
        COUNTIN,
        IFELSE,
        IF,
        ELSE,
        G,
        L,
        EQ,
        NEQ,
        GEQ,
        LEQ,
        SEMICOLON,
        OPENBRACKET,
        CLOSEBRACKET,
        OPENBRACE,
        CLOSEBRACE,
        WHILE,
        NOT,
        OR,
        AND,
        MATCH
    }

    abstract class ExpressionBase
    {
        public string text { get; set; }

        public ExpressionType type { get; set; }
        public int priority { get; set; }

        public ExpressionBase(string text = " ", ExpressionType type = ExpressionType.VARIABLE, int priority = 6)
        {
            this.text = text;
            this.type = type;
            this.priority = priority;
        }

        public abstract ExpressionBase Solve(ref Context context);

    }
    class ConstantExpression : ExpressionBase {
        public ConstantExpression(string text1)
        {
            type = ExpressionType.CONST;
            priority = 0;
            text = text1;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            return this;
        }
    }

    class VariableExpression : ExpressionBase {
        public VariableExpression(string text1)
        {
            type = ExpressionType.VARIABLE;
            priority = 0;
            text = text1;
        }
        public override ExpressionBase Solve(ref Context context) {
            string sValue;
            if (context.variables.TryGetValue(this.text, out sValue))
            {

            }
            else
            {
                sValue = "0";
            }
            return new ConstantExpression(sValue);
        }
    }

    abstract class UnaryExpression : ExpressionBase {
        public ExpressionBase Operand1 { get; set; }

        public override ExpressionBase Solve(ref Context context)
        {
            return Operand1;
        }
    }
    class NotExpression: UnaryExpression
    {
        public NotExpression()
        {
            type = ExpressionType.NOT;
            priority = 2;
            text = "!";
        }

        public override ExpressionBase Solve(ref Context context)
        {
            string boolean;
            ExpressionBase value1 = Operand1.Solve(ref context);
            if (value1.text == "True")
            {
                boolean = "False";
            }
            else
            {
                boolean = "True";
            }
            return new ConstantExpression(boolean);
        }
    }
    class VarExpression : UnaryExpression
    {
        public VarExpression()
        {
            type = ExpressionType.VAR;
            priority = 1;
            text = "var";
        }

        public override ExpressionBase Solve(ref Context context)
        {
            context.variables.Add(Operand1.text, "0");
            List<string> temp=new List<string>();
            if (context.layears.TryGetValue(context.layear, out temp))
            {
                context.layears[context.layear].Add(Operand1.text);
            }
            else
            {
                temp = new List<string>();
                temp.Add(Operand1.text);
                context.layears.Add(context.layear, temp);
            }
            return Operand1;
        }
    }
    abstract class BinaryExpression : UnaryExpression {
        public ExpressionBase Operand2 { get; set; }
    }
    class AssignmentExpression : BinaryExpression
    {
        public AssignmentExpression()
        {
            text = "=";
            priority = 20;
            type = ExpressionType.ASSIGNMENT;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value2 = Operand2.Solve(ref context);
            if (context.variables.ContainsKey(Operand1.text))
            {
                context.variables[Operand1.text] = value2.text;
            }
            else
            {
                if (Operand1.type == ExpressionType.VAR)
                {
                    ExpressionBase value1 = Operand1.Solve(ref context);
                    context.variables[value1.text] = value2.text;
                }
                else
                {
                    throw new Exception("Неможливо присвоїти");
                }
            }
            return value2;
        }
    }
    class PlusExpression : BinaryExpression
    {
        public PlusExpression()
        {
            text = "+";
            priority = 5;
            type = ExpressionType.PLUS;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            double op1, op2;
            if (Double.TryParse(value1.text, out op1)) {

            }
            if (Double.TryParse(value2.text, out op2)) {

            }
            double sum = op1 + op2;
            return new ConstantExpression(Convert.ToString(sum));
        }
    }
    class MinusExpression : BinaryExpression {
        public MinusExpression()
        {
            text = "-";
            priority = 5;
            type = ExpressionType.MINUS;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            double op1, op2;
            if (Double.TryParse(value1.text, out op1))
            {

            }
            if (Double.TryParse(value2.text, out op2))
            {

            }
            double sum = op1 - op2;
            return new ConstantExpression(Convert.ToString(sum));
        }
    }
    class MultiplyExpression : BinaryExpression {
        public MultiplyExpression()
        {
            text = "*";
            priority = 4;
            type = ExpressionType.MULTIPLY;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            double op1, op2;
            if (Double.TryParse(value1.text, out op1))
            {

            }
            if (Double.TryParse(value2.text, out op2))
            {

            }
            double sum = op1 * op2;
            return new ConstantExpression(Convert.ToString(sum));
        }
    }
    class DivisionExpression : BinaryExpression {
        public DivisionExpression()
        {
            text = "/";
            priority = 4;
            type = ExpressionType.DEVIDE;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            double op1, op2;
            if (Double.TryParse(value1.text, out op1))
            {

            }
            if (Double.TryParse(value2.text, out op2))
            {

            }
            double sum = op1 / op2;
            return new ConstantExpression(Convert.ToString(sum));
        }
    }
    class WhileExpression : BinaryExpression {
        public WhileExpression()
        {
            text = "while";
            priority = 14;
            type = ExpressionType.WHILE;
        }

        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase Boolean = Operand1.Solve(ref context);
            while (Boolean.text == "True")
            {
                Operand2.Solve(ref context);
                Boolean = Operand1.Solve(ref context);
            }
            return this;
        }
    }


    class LetterExpression : BinaryExpression {
        public LetterExpression()
        {
            text = "letter";
            priority = 9;
            type = ExpressionType.LETTER;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            StringBuilder pattern = new StringBuilder(@"[a-zA-Z]{", 10000);
            pattern.Append(value1.text).Append(',').Append(value2.text).Append('}');
            string res = pattern.ToString();
            return new ConstantExpression(res);
        }

    }

    class ConcatExpression: BinaryExpression{
        public ConcatExpression()
        {
            text = "|";
            priority = 10;
            type = ExpressionType.CONCAT;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            string res = value1.text + value2.text;
            return new ConstantExpression(res);
        }
    }
    class IsInExpression : BinaryExpression {
        public IsInExpression()
        {
            text = "is_in";
            priority = 11;
            type = ExpressionType.ISIN;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            StringBuilder sb = new StringBuilder();
            sb.Append(value2.text);
            Regex re = new Regex(sb.ToString());
            bool match = re.IsMatch(value1.text);
            string smatch = Convert.ToString(match);
            return new ConstantExpression(smatch);
        }
    }
    class CountInExpression : BinaryExpression {
        public CountInExpression()
        {
            text = "count_in";
            priority = 11;
            type = ExpressionType.COUNTIN;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            StringBuilder sb = new StringBuilder();
            sb.Append(value2.text);
            Regex re = new Regex(sb.ToString());
            MatchCollection matches = re.Matches(value1.text);
            int count = matches.Count;
            return new ConstantExpression(count.ToString());
        }
    }
    class MatchExpression : BinaryExpression
    {
        public ExpressionBase Operand3 { get; set; }
        public MatchExpression()
        {
            text = "match";
            priority = 11;
            type = ExpressionType.MATCH;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            ExpressionBase value3 = Operand3.Solve(ref context);
            StringBuilder sb = new StringBuilder();
            sb.Append(value2.text);
            Regex re = new Regex(sb.ToString());
            MatchCollection matches = re.Matches(value1.text);
            int index = 0;
            if (int.TryParse(value3.text, out index))
            {
                if(index>=0&&index<matches.Count)
                    return new ConstantExpression(matches[index].ToString());
                else
                    return new ConstantExpression(" ");
            }
            else
            {
                return new ConstantExpression(" ");
            }
        }
    }
    class PrintExpression : UnaryExpression{
           public PrintExpression(){
            text = "print";
            priority = 12;
            type = ExpressionType.PRINT;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            //Console.Write(value1.text);
            context.output += value1.text+"\r\n";
            return value1;
        }
    }
    class IfElseExpression : BinaryExpression {
        public ExpressionBase Operand3 { get; set; }
        public IfElseExpression()
        {
            text = "ifelse";
            priority = 15;
            type = ExpressionType.IFELSE;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1=Operand1.Solve(ref context);
            if (value1.text == "True")
            {
                Operand2.Solve(ref context);
            }else if (Operand3 != null)
            {
                Operand3.Solve(ref context);
            }
            return this;
        }
    }
    class IfExpression : UnaryExpression {
        public IfExpression()
        {
            text = "if";
            priority = 14;
            type = ExpressionType.IF;
        }

        public override ExpressionBase Solve(ref Context context)
        {
            Operand1.Solve(ref context);
            return Operand1;
        }
    }
    class ElseExpression : UnaryExpression
    {
        public ElseExpression()
        {
            text = "else";
            priority = 14;
            type = ExpressionType.ELSE;
        }

        public override ExpressionBase Solve(ref Context context)
        {
            Operand1.Solve(ref context);
            return Operand1;
        }
    }
    class EqualExpression : BinaryExpression {
        public EqualExpression()
        {
            text = "==";
            priority = 6;
            type = ExpressionType.EQ;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            if (Convert.ToDouble(value1.text) == Convert.ToDouble(value2.text))
            {
                return new ConstantExpression("True");
            }
            else
            {
                return new ConstantExpression("False");
            }
        }
    }
    class AndExpression : BinaryExpression
    {
        public AndExpression()
        {
            text = "&&";
            priority = 7;
            type = ExpressionType.AND;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            if (value1.text == "False")
            {
                Console.Out.WriteLine(this.text + " opt");
                return new ConstantExpression("False");
            }
            ExpressionBase value2 = Operand2.Solve(ref context);
            if (value2.text == "True")
            {
                return new ConstantExpression("True");
            }
            else
            {
                return new ConstantExpression("False");
            }
        }
    }

    class OrExpression : BinaryExpression
    {
        public OrExpression()
        {
            text = "||";
            priority = 7;
            type = ExpressionType.OR;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            if (value1.text == "True")
            {
                Console.Out.WriteLine(this.text + " opt");
                return new ConstantExpression("True");
            }
            ExpressionBase value2 = Operand2.Solve(ref context);
            if (value2.text == "True")
            {
                return new ConstantExpression("True");
            }
            else
            {
                return new ConstantExpression("False");
            }
        }
    }

    class GreaterExpression : BinaryExpression
    {
        public GreaterExpression()
        {
            text = ">";
            priority = 6;
            type = ExpressionType.G;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1=Operand1.Solve(ref context);
            ExpressionBase value2=Operand2.Solve(ref context);

            if (Convert.ToDouble(value1.text) > Convert.ToDouble(value2.text))
            {
                return new ConstantExpression("True");
            }
            else
            {
                return new ConstantExpression("False");
            }
        }
    }

    class LesserExpression : BinaryExpression
    {
        public LesserExpression()
        {
            text = "<";
            priority = 6;
            type = ExpressionType.EQ;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            if (Convert.ToDouble(value1.text) < Convert.ToDouble(value2.text))
            {
                return new ConstantExpression("True");
            }
            else
            {
                return new ConstantExpression("False");
            }
        }
    }
    class LesserEqualExpression : BinaryExpression
    {
        public LesserEqualExpression()
        {
            text = "<=";
            priority = 6;
            type = ExpressionType.LEQ;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            if (Convert.ToDouble(value1.text) <= Convert.ToDouble(value2.text))
            {
                return new ConstantExpression("True");
            }
            else
            {
                return new ConstantExpression("False");
            }
        }
    }
    class GreaterEqualExpression : BinaryExpression
    {
        public GreaterEqualExpression()
        {
            text = ">=";
            priority = 6;
            type = ExpressionType.GEQ;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            if (Convert.ToDouble(value1.text) >= Convert.ToDouble(value2.text))
            {
                return new ConstantExpression("True");
            }
            else
            {
                return new ConstantExpression("False");
            }
        }
    }
    class NotEqualExpression : BinaryExpression
    {
        public NotEqualExpression()
        {
            text = "!=";
            priority = 6;
            type = ExpressionType.NEQ;
        }
        public override ExpressionBase Solve(ref Context context)
        {
            ExpressionBase value1 = Operand1.Solve(ref context);
            ExpressionBase value2 = Operand2.Solve(ref context);
            if (Convert.ToDouble(value1.text) != Convert.ToDouble(value2.text))
            {
                return new ConstantExpression("True");
            }
            else
            {
                return new ConstantExpression("False");
            }
        }
    }
    class SemicolonExpression:ExpressionBase{
        public SemicolonExpression()
        {
            text = ";";
            type = ExpressionType.SEMICOLON;
        }

        public override ExpressionBase Solve(ref Context context)
        {
            return this;
        }
    }
    class OpenBracketExpression : ExpressionBase
    {
        public OpenBracketExpression()
        {
            text = "(";
            type = ExpressionType.OPENBRACKET;
            priority = 20;
        }

        public override ExpressionBase Solve(ref Context context)
        {
            return this;
        }
    }
    class CloseBracketExpression : ExpressionBase
    {
        public CloseBracketExpression()
        {
            text = ")";
            type = ExpressionType.CLOSEBRACKET;
            priority = 20;
        }

        public override ExpressionBase Solve(ref Context context)
        {
            return this;
        }
    }
    class OpenBraceExpression : ExpressionBase
    {
        public List<ExpressionBase> OperandList { get; set; } = new List<ExpressionBase>();
        public OpenBraceExpression()
        {
            type = ExpressionType.OPENBRACE;
            priority = 13;
            text = "{";
        }
        public override ExpressionBase Solve(ref Context context)
        {
            context.layear++;
            foreach (ExpressionBase expression in OperandList)
            {
                expression.Solve(ref context);
            }
            List<string> temp = new List<string>();
            if (context.layears.TryGetValue(context.layear, out temp)){
                for (int i = 0; i < temp.Count; i++)
                {
                    context.variables.Remove(temp[i]);
                }
                context.layears.Remove(context.layear);
            }
            context.layear--;
            return this;
        }
        public void SolveWithoutExit(ref Context context)
        {
            context.layear++;
            foreach (ExpressionBase expression in OperandList)
            {
                expression.Solve(ref context);
            }
        }
        public void Exit(ref Context context)
        {
            List<string> temp = new List<string>();
            if (context.layears.TryGetValue(context.layear, out temp))
            {
                for (int i = 0; i < temp.Count; i++)
                {
                    context.variables.Remove(temp[i]);
                }
                context.layears.Remove(context.layear);
            }
            context.layear--;
        }
    }
    class CloseBraceExpression : ExpressionBase
    {
        public CloseBraceExpression()
        {
            text = "}";
            type = ExpressionType.CLOSEBRACE;
        }

        public override ExpressionBase Solve(ref Context context)
        {
            return this;
        }
    }
}
