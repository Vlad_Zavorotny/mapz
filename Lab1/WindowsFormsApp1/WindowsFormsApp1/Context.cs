﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Context
    {
        public Dictionary<string, string> variables { get; } = new Dictionary<string, string>();
        public int layear { set; get; } = 0;
        public Dictionary<int, List<string>> layears { get; } = new Dictionary<int, List<string>>();
        public string output { set; get; }
    }
}
