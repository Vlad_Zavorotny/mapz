﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Parser
    {
        public ExpressionBase CreateTree(ref List<ExpressionBase> expressions)
        {
            Stack<ExpressionBase> varStack=new Stack<ExpressionBase>();
            Stack<ExpressionBase> operatorStack=new Stack<ExpressionBase>();
            ExpressionBase temp=expressions[0];
            while (temp != null && temp.type != ExpressionType.SEMICOLON)
            {
                if(temp.type==ExpressionType.VARIABLE|| temp.type == ExpressionType.CONST)
                {
                    varStack.Push(temp);
                }else if (temp.type==ExpressionType.OPENBRACKET)
                {
                    operatorStack.Push(temp);
                }else if (temp.type == ExpressionType.CLOSEBRACKET)
                {
                    while (operatorStack.Peek().type != ExpressionType.OPENBRACKET)
                    {
                        InsertOperator(ref varStack, ref operatorStack);
                    }
                    operatorStack.Pop();
                }else if (temp.type==ExpressionType.OPENBRACE)
                {
                    InsertBlock(ref expressions, ref varStack);
                }
                else if (operatorStack.Count != 0 && operatorStack.Peek().priority <= temp.priority)
                {
                    if(!(operatorStack.Peek().priority==temp.priority && temp.type==ExpressionType.ASSIGNMENT))
                        InsertOperator(ref varStack, ref operatorStack);
                    operatorStack.Push(temp);
                }
                else
                {
                    operatorStack.Push(temp);
                }
                expressions.RemoveAt(0);
                temp = expressions[0];
            }
            if (expressions[0] != null)
                expressions.RemoveAt(0);
            while (operatorStack.Count != 0)
            {
                InsertOperator(ref varStack, ref operatorStack);
            }
            return varStack.Peek();
        }
        public void Execute(ref Context context,ref List<ExpressionBase> expressions)
        {
            while (expressions.Count != 0)
            {
                ExpressionBase tempTree = CreateTree(ref expressions);
                tempTree.Solve(ref context);
            }
        }
        private void InsertOperator(ref Stack<ExpressionBase> varStack,ref Stack<ExpressionBase> operatorStack)
        {

            switch (operatorStack.Peek())
            {
                case IfElseExpression ex3:

                    if (varStack.Peek().type == ExpressionType.ELSE)
                    {
                        ex3.Operand3 = varStack.Pop();
                    }
                    else
                    {
                        ex3.Operand3 = null;
                    }
                    ex3.Operand2 = varStack.Pop();
                    ex3.Operand1 = varStack.Pop();
                    varStack.Push(ex3);
                    break;
                case MatchExpression ex3:
                    ex3.Operand3 = varStack.Pop();  
                    ex3.Operand2 = varStack.Pop();
                    ex3.Operand1 = varStack.Pop();
                    varStack.Push(ex3);
                    break;
                case BinaryExpression ex2:
                        ex2.Operand2 = varStack.Pop();
                        ex2.Operand1 = varStack.Pop();
                 
                    varStack.Push(ex2);
                    break;
                case UnaryExpression ex1:
                    ex1.Operand1 = varStack.Pop();
                    varStack.Push(ex1);
                    break;
            }
            operatorStack.Pop();
        }
        private void InsertBlock(ref List<ExpressionBase> expressions, ref Stack<ExpressionBase> varStack)
        {
            ExpressionBase temp = expressions[0];
            expressions.RemoveAt(0);
            switch (temp)
            {
                case OpenBraceExpression block:
                    while (expressions[0].type != ExpressionType.CLOSEBRACE)
                    {
                        ExpressionBase tempTree = CreateTree(ref expressions);
                        block.OperandList.Add(tempTree);
                    }
                    varStack.Push(block);
                    break;
            }
        }
        public List<ExpressionBase> Optimization(List<ExpressionBase> treeList,ref Context context)
        {
            List<int> toDelete = new List<int>();
            List<ExpressionBase> resultList = new List<ExpressionBase>();
            for (int i = 0; i < treeList.Count; i++)
            {
                bool addDelete = false;
                ExpressionBase current = treeList[i];
                int countR = 0;
                int gap = 0;
                switch (current)
                {
                    case AssignmentExpression assignment:
                        ExpressionBase operand = AssignmentOperand(assignment.Operand1);
                        ExpressionBase constant = assignment.Operand2;
                        bool isR = true;
                        int m_layer = context.layear;
                        List<string> vars=new List<string>();
                        if (context.layears.TryGetValue(m_layer, out vars)){
                            if (vars.Contains(operand.text)) 
                            for (int j = i + 1; j < treeList.Count; j++)
                            {
                                isR = isRequired(operand.text, treeList[j], null);

                                if (isR == false)
                                {
                                    switch (treeList[j])
                                    {
                                        case AssignmentExpression ex2:
                                            if (operand.text == AssignmentOperand(ex2.Operand1).text)
                                            {
                                                constant = ex2.Operand2;
                                                treeList.RemoveAt(j);
                                                j--;
                                                gap++;
                                            }
                                            else
                                            {
                                                treeList[j] = Swap(operand, constant, treeList[j], ref context);
                                            }
                                            break;

                                            default:
                                            treeList[j] = Swap(operand, constant, treeList[j], ref context);
                                            break;
                                    }
                                    countR++;

                                }
                                else
                                {
                                    switch (treeList[j])
                                    {
                                        case AssignmentExpression ex2:
                                            if (ex2.Operand2.type != ExpressionType.ASSIGNMENT)
                                            {
                                                ex2.Operand2 = Swap(operand, constant, ex2.Operand2, ref context);
                                                constant = ex2.Operand2.Solve(ref context);
                                                countR++;
                                            }
                                            else
                                            {
                                                constant = ex2.Operand2.Solve(ref context);
                                            }
                                            break;
                                            case WhileExpression whileexp:
                                                ConstantExpression be = (ConstantExpression)whileexp.Operand1.Solve(ref context);
                                                switch (whileexp.Operand2)
                                                {
                                                    case OpenBraceExpression brace:
                                                        brace.SolveWithoutExit(ref context);
                                                        brace.OperandList = Optimization(brace.OperandList, ref context);
                                                        brace.Exit(ref context);

                                                        constant = operand.Solve(ref context);
                                                        break;
                                                }
                                                break;
                                            case OpenBraceExpression brace:
                                                brace.SolveWithoutExit(ref context);
                                                brace.OperandList = Optimization(brace.OperandList, ref context);
                                                brace.Exit(ref context);
                                                constant = operand.Solve(ref context);
                                                break;
                                            case IfElseExpression ex3:
                                                switch (ex3.Operand2)
                                                {
                                                    case IfExpression ifexp:
                                                        switch (ifexp.Operand1)
                                                        {
                                                            case OpenBraceExpression brace:
                                                                brace.SolveWithoutExit(ref context);
                                                                brace.OperandList = Optimization(brace.OperandList, ref context);
                                                                brace.Exit(ref context);
                                                                constant = operand.Solve(ref context);
                                                                break;
                                                        }
                                                        break;
                                                }
                                                if (ex3.Operand3 != null)
                                                {
                                                    switch (ex3.Operand1)
                                                    {
                                                        case ElseExpression ifexp:
                                                            switch (ifexp.Operand1)
                                                            {
                                                                case OpenBraceExpression brace:
                                                                    brace.SolveWithoutExit(ref context);
                                                                    brace.OperandList = Optimization(brace.OperandList, ref context);
                                                                    brace.Exit(ref context);
                                                                    //constant = operand.Solve(ref context);
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                }
                                                break;
                                        }

                                }
                            }
                        }
                        break;
                    case  VarExpression var:
                        operand =var.Operand1;
                        constant = new ConstantExpression("0");
                        isR = true;
                        for (int j = i + 1; j < treeList.Count; j++)
                        {
                            isR = isRequired(operand.text, treeList[j], null);
                            if (isR == false)
                            {
                                switch (treeList[j])
                                {
                                    case AssignmentExpression ex2:
                                        if (operand.text == AssignmentOperand(ex2.Operand1).text)
                                        {
                                        constant = ex2.Operand2;
                                        treeList.RemoveAt(j);
                                        j--;
                                        gap++;
                                        }
                                        else
                                        {
                                            treeList[j] = Swap(operand, constant, treeList[j], ref context);
                                        }
                                        break;
                                    default:
                                        treeList[j] = Swap(operand, constant, treeList[j], ref context);
                                        break;
                                }
                                    countR++;
                            
                            }
                            else
                            {
                                switch (treeList[j])
                                {
                                    case AssignmentExpression ex2:
                                    if (ex2.Operand2.type != ExpressionType.ASSIGNMENT)
                                    {
                                        ex2.Operand2 = Swap(operand, constant, ex2.Operand2, ref context);
                                        constant = ex2.Operand2.Solve(ref context);
                                        countR++;
                                    }
                                    else
                                    {
                                        constant = ex2.Operand2.Solve(ref context);
                                    }
                                     break;
                                }

                            }
                        }
                        break;
                    case OpenBraceExpression brace:
                        brace.SolveWithoutExit(ref context);
                        brace.OperandList = Optimization(brace.OperandList, ref context);
                        brace.Exit(ref context);
                        break;
                    case IfElseExpression ex3:
                        switch (ex3.Operand2)
                        {
                            case IfExpression ifexp:
                                switch (ifexp.Operand1)
                                {
                                    case OpenBraceExpression brace:
                                        brace.SolveWithoutExit(ref context);
                                        brace.OperandList = Optimization(brace.OperandList, ref context);
                                        brace.Exit(ref context);
                                        break;
                                }  
                                break;
                        }
                        if (ex3.Operand3 != null)
                        {
                            switch (ex3.Operand1)
                            {
                                case ElseExpression ifexp:
                                    switch (ifexp.Operand1)
                                    {
                                        case OpenBraceExpression brace:
                                            brace.SolveWithoutExit(ref context);
                                            brace.OperandList = Optimization(brace.OperandList, ref context);
                                            brace.Exit(ref context);
                                            break;
                                    }
                                    break;
                            }
                        }
                        break;
                    case WhileExpression whileexp:
                        switch (whileexp.Operand2)
                        {
                            case OpenBraceExpression brace:
                                brace.SolveWithoutExit(ref context);
                                brace.OperandList = Optimization(brace.OperandList, ref context);
                                brace.Exit(ref context);
                                break;
                        }
                        break;
                }
                if (countR == treeList.Count - i - 1+gap&&countR!=0)
                {
                    if (addDelete == false)
                    {
                        toDelete.Add(i);
                        addDelete = true;
                    }

                }
            }
            toDelete.Sort();
            for(int i = toDelete.Count-1; i >=0; i--)
            {
                treeList.RemoveAt(toDelete[i]);
            }
            return treeList;

        }
        private ExpressionBase AssignmentOperand(ExpressionBase Operand1)
        {
            switch (Operand1)
            {
                case VariableExpression variable:
                    return variable;
                    
                case VarExpression var:
                    return var.Operand1;
                    
            }
            return Operand1;
        }
        private bool isRequired(string name, ExpressionBase root,ExpressionBase parent)
        {
            bool temp = false;
            switch (root)
            {
                case AssignmentExpression assignment:
                    temp= isIn(name, assignment.Operand1);
                    bool temp1 = isIn(name, assignment.Operand2);
                    if (temp && temp1)
                        return true;
                    if (parent != null && parent.type == ExpressionType.ASSIGNMENT)
                        return temp;
                    break;
                case OpenBraceExpression exlist:
                    //foreach (ExpressionBase op in exlist.OperandList)
                    //{
                    //    temp = isRequired(name, op,root);
                    //    if (temp == true)
                    //        return temp;
                    //}
                    temp = isIn(name, exlist);
                    if (temp == true)
                        return temp;
                    break;
                
                case IfElseExpression ex3:
                    temp = isRequired(name, ex3.Operand1,root);
                    if (temp == true)
                        return temp;
                    temp = isRequired(name, ex3.Operand2,root);
                    if (temp == true)
                        return temp;
                    temp = isRequired(name, ex3.Operand3,root);
                    if (temp == true)
                        return temp;
                    break;
                case BinaryExpression ex2:
                    temp = isRequired(name, ex2.Operand1,root);
                    if (temp == true)
                        return temp;
                    temp = isRequired(name, ex2.Operand2,root);
                    if (temp == true)
                        return temp;
                    break;
                case UnaryExpression ex1:
                    temp = isRequired(name, ex1.Operand1,root);
                    if (temp == true)
                        return temp;
                    break;
            }
            return false;
        }


        private bool isIn(string name, ExpressionBase root)
        {
            bool temp = false;
            switch (root)
            {
                case VariableExpression variable:
                    if (variable.text == name)
                        return true;
                    break;
                case OpenBraceExpression exlist:
                    foreach (ExpressionBase op in exlist.OperandList)
                    {
                        temp = isIn(name, op);
                        if (temp == true)
                            return temp;
                    }
                    break;
                case IfElseExpression ex3:
                    temp = isIn(name, ex3.Operand1);
                    if (temp == true)
                        return temp;
                    temp = isIn(name, ex3.Operand2);
                    if (temp == true)
                        return temp;
                    temp = isIn(name, ex3.Operand3);
                    if (temp == true)
                        return temp;
                    break;
                case BinaryExpression ex2:
                    temp = isIn(name, ex2.Operand1);
                    if (temp == true)
                        return temp;
                    temp = isIn(name, ex2.Operand2);
                    if (temp == true)
                        return temp;
                    break;
                case UnaryExpression ex1:
                    temp = isIn(name, ex1.Operand1);
                    if (temp == true)
                        return temp;
                    break;
            }
            return false;
        }

        private ExpressionBase Swap(ExpressionBase variable,ExpressionBase constant, ExpressionBase root,ref Context context)
        {
            switch (root)
            {
                case VariableExpression m_var:
                    if (variable.text == m_var.text)
                    {
                        return constant.Solve(ref context); ;
                    }
                    break;
                case OpenBraceExpression exlist:
                    for(int i = 0; i < exlist.OperandList.Count; i++)
                    {
                        exlist.OperandList[i] = Swap(variable,constant, exlist.OperandList[i], ref context);
                    }
                    break;
                case IfElseExpression ex3:
                    ex3.Operand1 = Swap(variable,constant, ex3.Operand1,ref context);
                    ex3.Operand2 = Swap(variable,constant, ex3.Operand2, ref context);
                    ex3.Operand3 = Swap(variable,constant, ex3.Operand3, ref context);
                    break;
                case BinaryExpression ex2:
                    ex2.Operand1 =  Swap(variable,constant, ex2.Operand1, ref context);
                    ex2.Operand2 = Swap(variable,constant, ex2.Operand2, ref context);
                    break;
                case UnaryExpression ex1:
                    ex1.Operand1 = Swap(variable,constant, ex1.Operand1, ref context);
                    break;
            }
            return root;
        }
    }


}
