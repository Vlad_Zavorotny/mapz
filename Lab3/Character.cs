﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Runtime.Serialization.Formatters.Binary;

namespace GameNameSpace
{

    [Serializable]
    public abstract class BaseObject {
        public abstract void atack(PlayerCharacter character, int damage = 0);
    }

    [Serializable]
    public abstract class GameObject : BaseObject,ICloneable
    {
        public string name { get; set; }
        public int strength { get; set; }
        public int perception { get; set; }
        public int endurance { get; set; }
        public int charisma { get; set; }
        public int intelligence { get; set; }
        public int agility { get; set; }
        public int luck { get; set; }
        [NonSerialized]
        public BitmapImage image;
        public GameObject()
        {
            name = "DefaultObject";
            strength = 0;
            perception = 0;
            endurance = 0;
            charisma = 0;
            intelligence = 0;
            agility = 0;
            luck = 0;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    [Serializable]
    public abstract class Character:GameObject,ICloneable,IElement
    {
        public int maxHP { get; set; }
        public int currentHP { get; set; }
        public int maxMP { get; set; }
        public int currentMP { get; set; }
        public MapCell position { get; set; }
        public Character()
        {
            name = "Character";
            strength = 3;
            perception = 3;
            endurance = 3;
            charisma = 3;
            intelligence = 3;
            agility = 3;
            luck = 3;
            maxHP = 100;
            currentHP = 100;
            maxMP = 100;
            currentMP = 100; 
        }
        
        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
        }

        public void moveLeft(Map map)
        {
                map.cells[this.position.positionX, this.position.positionY - 1].cellType = MapCellType.ENEMY;
                map.cells[this.position.positionX, this.position.positionY].cellObject = null;
                map.cells[this.position.positionX, this.position.positionY].cellType = MapCellType.FREE;
                this.position = map.cells[this.position.positionX, this.position.positionY - 1];
                map.cells[this.position.positionX, this.position.positionY].cellObject = this;

        }

        public void moveRight( Map map)
        {
                map.cells[this.position.positionX, this.position.positionY + 1].cellType = MapCellType.ENEMY;
                map.cells[this.position.positionX, this.position.positionY].cellObject = null;
                map.cells[this.position.positionX, this.position.positionY].cellType = MapCellType.FREE;
                this.position = map.cells[this.position.positionX, this.position.positionY + 1];
                map.cells[this.position.positionX, this.position.positionY].cellObject = this;

        }

        public void moveUp(Map map)
        {
                map.cells[this.position.positionX-1, this.position.positionY].cellType = MapCellType.ENEMY;
                map.cells[this.position.positionX, this.position.positionY].cellObject = null;
                map.cells[this.position.positionX, this.position.positionY].cellType = MapCellType.FREE;
                this.position = map.cells[this.position.positionX - 1, this.position.positionY];
                map.cells[this.position.positionX, this.position.positionY].cellObject = this;

        }

        public void moveDown(Map map)
        {
                map.cells[this.position.positionX+1, this.position.positionY].cellType = MapCellType.ENEMY;
                map.cells[this.position.positionX, this.position.positionY].cellObject = null;
                map.cells[this.position.positionX, this.position.positionY].cellType = MapCellType.FREE;
                this.position = map.cells[this.position.positionX + 1, this.position.positionY];
                map.cells[this.position.positionX, this.position.positionY].cellObject = this;
                this.position = this.position;

        }
    }
    [Serializable]
    public class NeutralCharacter : Character
    {
        public NeutralCharacter()
        {
            name = "Neutral Character";
            image = ImageFactory.CreateNeutralImage();

        }

        public override void atack(PlayerCharacter character, int damage = 0)
        {
            character.playearState.currentHealthPoints -= damage;
        }
    }
    [Serializable]
    public class EnemyCharacter : Character
    {
        public EnemyCharacter()
        {
            name = "Enemy Character";
            image = ImageFactory.CreateEnemyImage();
        }
        public override void atack(PlayerCharacter character, int damage = 0)
        {
            character.playearState.currentHealthPoints -= damage;
        }

    }
    [Serializable]
    public class WeakNeutralCharacter: NeutralCharacter
    {
        public WeakNeutralCharacter()
        {
            name = "Weak Neutral Character";
            strength = 1;
            perception = 1;
            endurance = 1;
            charisma = 1;
            intelligence = 1;
            agility = 1;
            luck = 1;
            maxHP = 50;
            currentHP = 50;
            maxMP = 50;
            currentMP = 50;
        }
    }
    [Serializable]
    public class WeakEnemyCharacter: EnemyCharacter
    {
        public WeakEnemyCharacter()
        {
            name = "Weak Enemy Character";
            strength = 1;
            perception = 1;
            endurance = 1;
            charisma = 1;
            intelligence = 1;
            agility = 1;
            luck = 1;
            maxHP = 50;
            currentHP = 50;
            maxMP = 50;
            currentMP = 50;
        }
    }
    [Serializable]
    public class StrongNeutralCharacter:NeutralCharacter
    {
        public StrongNeutralCharacter()
        {
            name = "Strong Neutral Character";
            strength = 5;
            perception = 5;
            endurance = 5;
            charisma = 5;
            intelligence = 5;
            agility = 5;
            luck = 5;
            maxHP = 150;
            currentHP = 150;
            maxMP = 150;
            currentMP = 150;
        }
    }
    [Serializable]
    public class StrongEnemyCharacter:EnemyCharacter
    {
        public StrongEnemyCharacter()
        {
            name = "Strong Enemy Character";
            strength = 5;
            perception = 5;
            endurance = 5;
            charisma = 5;
            intelligence = 5;
            agility = 5;
            luck = 5;
            maxHP = 150;
            currentHP = 150;
            maxMP = 150;
            currentMP = 150;
        }
    }
    [Serializable]
    public class PlayearCharacterState:ICloneable
    {
        public int maxHealthPoints { set; get; }
        public int currentHealthPoints { set; get; }
        public int maxManaPoints { set; get; }
        public int currentManaPoints { get; set; }
        public int level { get; set; }
        public int currentExp { get; set; }
        public int levelExp { get; set; }
        public int strength { get; set; }
        public int perception { get; set; }
        public int endurance { get; set; }
        public int charisma { get; set; }
        public int intelligence { get; set; }
        public int agility { get; set; }
        public int luck { get; set; }
        public Clothing[] activeArmorSlots { get; set; }
        public Weapon activeWeaponSlot { get; set; }
        public MapCell position { get; set; }
        public Inventory inventory { get; set; }
        public PlayearCharacterState(){
            maxHealthPoints = 100;
            currentHealthPoints = 100;
            maxManaPoints = 100;
            currentManaPoints = 100;
            level = 1;
            currentExp = 0;
            levelExp = 100;
            strength = 1;
            perception = 1;
            endurance = 1;
            charisma = 1;
            intelligence = 1;
            agility = 1;
            luck = 1;
            position = null;
            activeWeaponSlot=null;
            activeArmorSlots = new Clothing[4];
            inventory = new Inventory();
        }

        public object Clone()
        {
            PlayearCharacterState state = new PlayearCharacterState();
            state.maxHealthPoints = maxHealthPoints;
            state.currentHealthPoints= currentHealthPoints;
            state.maxManaPoints= maxManaPoints;
            state.currentManaPoints= currentManaPoints;
            state.level= level;
            state.currentExp= currentExp;
            state.levelExp=levelExp;
            state.strength= strength;
            state.perception= perception;
            state.endurance= endurance;
            state.charisma= charisma;
            state.intelligence= intelligence;
            state.agility= agility;
            state.luck= luck;
            state.position =(MapCell)position.Clone() ;
            state.activeArmorSlots = new Clothing[4];
            for(int i = 0; i < activeArmorSlots.Length; i++)
            {
                if(activeArmorSlots[i]!=null)
                    state.activeArmorSlots[i] = (Clothing)activeArmorSlots[i].Clone();
            }
            if(activeWeaponSlot!=null)
                state.activeWeaponSlot = (Weapon)activeWeaponSlot.Clone();
            state.inventory = (Inventory)this.inventory.Clone();
            return state;
        }
    }
    public class PlayerCharacter
    {
        private static PlayerCharacter playearCharacterInstance = null;
        private static object locker = new object();
        public PlayearCharacterState playearState { get; set; }
        private PlayerCharacter()
        {
            playearState = new PlayearCharacterState();
        }
        public static PlayerCharacter GetInstance()
        {
            lock (locker)
            {
                if (playearCharacterInstance == null)
                {
                    playearCharacterInstance = new PlayerCharacter();
                }
            }
            return playearCharacterInstance;
        }
        public bool moveLeft(Map map)
        {
            MapCell pos = PlayerCharacter.GetInstance().playearState.position;
            if (pos.positionY > 0 && map.cells[pos.positionX, pos.positionY - 1].cellType != MapCellType.NOTFREE)
            {
                pos = map.cells[pos.positionX, pos.positionY - 1];
                PlayerCharacter.GetInstance().playearState.position = pos;
                return true;
            }
            return false;
        }
        public bool moveRight(Map map)
        {
            MapCell pos = PlayerCharacter.GetInstance().playearState.position;
            if (pos.positionY < map.maxY - 1 && map.cells[pos.positionX, pos.positionY + 1].cellType != MapCellType.NOTFREE)
            {
                pos = map.cells[pos.positionX, pos.positionY + 1];
                PlayerCharacter.GetInstance().playearState.position = pos;
                return true;
            }
            return false;
        }
        public bool moveUp(Map map)
        {
            MapCell pos = PlayerCharacter.GetInstance().playearState.position;
            if (pos.positionX > 0 && map.cells[pos.positionX - 1, pos.positionY].cellType != MapCellType.NOTFREE)
            {
                pos = map.cells[pos.positionX - 1, pos.positionY];
                PlayerCharacter.GetInstance().playearState.position = pos;
                return true;
            }
            return false;
        }
        public bool moveDown(Map map)
        {
            MapCell pos = PlayerCharacter.GetInstance().playearState.position;
            if (pos.positionX < map.maxX - 1 && map.cells[pos.positionX + 1, pos.positionY].cellType != MapCellType.NOTFREE)
            {
                pos = map.cells[pos.positionX + 1, pos.positionY];
                PlayerCharacter.GetInstance().playearState.position = pos;
                return true;
            }
            return false;
        }
        public void atack(Character character, int damage=0)
        {
            character.currentHP -= damage;
        }
        public void levelUp()
        {
            playearState.level += 1;
            playearState.currentExp -= playearState.levelExp;
            playearState.strength += 1;
            playearState.perception += 1;
            playearState.endurance += 1;
            playearState.charisma += 1;
            playearState.intelligence += 1;
            playearState.agility += 1;
            playearState.luck += 1;
        }

    }
        public interface ICharacterFactory
        {
            NeutralCharacter GetNeutralCharacter();
            EnemyCharacter GetEnemyCharacter();
        }
        public class WeakCharacterFactory : ICharacterFactory
        {
            public NeutralCharacter GetNeutralCharacter()
            {
                return new WeakNeutralCharacter();
            }
            public EnemyCharacter GetEnemyCharacter()
            {
                return new WeakEnemyCharacter();
            }
        }
        public class MediumCharacterFactory : ICharacterFactory
        {
            public NeutralCharacter GetNeutralCharacter()
            {
                return new NeutralCharacter();
            }
            public EnemyCharacter GetEnemyCharacter()
            {
                return new EnemyCharacter();
            }
        }
        public class StrongCharacterFactory : ICharacterFactory
        {
            public NeutralCharacter GetNeutralCharacter()
            {
                return new StrongNeutralCharacter();
            }
            public EnemyCharacter GetEnemyCharacter()
            {
                return new StrongEnemyCharacter();
            }
        }
    
}
