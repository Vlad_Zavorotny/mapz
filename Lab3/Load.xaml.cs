﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameNameSpace
{
    /// <summary>
    /// Interaction logic for Load.xaml
    /// </summary>
    public partial class Load : Window
    {
        public string result { get; set; }
        public Load(string[] saves)
        {
            InitializeComponent();
            for(int i = 0; i < saves.Length; i++)
            {
                ComboBoxItem it = new ComboBoxItem();
                it.Content = saves[i];
                comboBox.Items.Add(it);
            }
        }
        public void LoadClick(object sender, EventArgs e)
        {
            string[]temp = comboBox.SelectedItem.ToString().Split('\\');
            result = temp[temp.Length - 1];
            this.DialogResult = true;
        }
    }

}
