﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace GameNameSpace
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        

        public Dictionary<Vector,Image> MapImages = new Dictionary<Vector,Image>();

        public Image heroImage { get; set; } = new Image();
        public Game game { get; set; }
        public CareTaker careTaker { get; set; }
        private Label currentState;
        public GameWindow()
        {
            InitializeComponent();
            GameContext state = new GameContext();
            state.player = new PlayearCharacterState();
            PlayerCharacter character = PlayerCharacter.GetInstance();
            character.playearState = state.player;
            MapDirector creator = new MapDirector();
            creator.SetMapBuilder(new TownMapBuilder());
            creator.ConstructMap();
            state.map= creator.GetMyMap(); 
            game = new Game(state);
            game.additionalDifficulty = 1;
            game.MakeHarder();
            careTaker = new CareTaker(game);
            update();
        }
        private Image showChar()
        {
            Image imageControl=heroImage;
            if (mapCanvas.Children.Contains(heroImage))
            {
                mapCanvas.Children.Remove(heroImage);
            }
            BitmapImage image = ImageFactory.CreateHeroImage();    
            imageControl.Source = image;
            imageControl.Width=70;
            imageControl.Height = 70;
            Canvas.SetLeft(imageControl, game.gameContext.player.position.positionY * 100);
            Canvas.SetTop(imageControl, game.gameContext.player.position.positionX * 100);
            mapCanvas.Children.Add(imageControl);
            return imageControl;
        }
        private void showMap()
        {
            foreach(Image img in MapImages.Values)
            {
                mapCanvas.Children.Remove(img);
            }
            MapImages.Clear();
            mapCanvas.Width = 100 * game.gameContext.map.maxX;
            mapCanvas.Height = 100 * game.gameContext.map.maxX;
            Rectangle rect = new Rectangle();
            rect.Width = mapCanvas.Width;
            rect.Height = mapCanvas.Height;
            rect.Fill = new SolidColorBrush(Colors.YellowGreen);
            Canvas.SetTop(rect, 0);
            Canvas.SetLeft(rect, 0);
            mapCanvas.Children.Add(rect);
            for(int i = 0; i < game.gameContext.map.maxX; i++)
            {
                for(int j = 0; j < game.gameContext.map.maxY; j++)
                {
                    
                    if (game.gameContext.map.cells[i, j].cellType != MapCellType.FREE)
                    {


                        if (game.gameContext.map.cells[i, j].cellType == MapCellType.START || game.gameContext.map.cells[i, j].cellType == MapCellType.END)
                        {
                            Shape shape;
                            shape = new Rectangle();
                            shape.Fill = new SolidColorBrush(Colors.Aqua);
                            shape.Width = 100;
                            shape.Height = 100;
                            Canvas.SetTop(shape, i * 100);
                            Canvas.SetLeft(shape, j * 100);
                            mapCanvas.Children.Add(shape);
                        }
                        else {

                            BitmapImage image;
                            Image imageControl=new Image();
                            if (game.gameContext.map.cells[i, j].cellType == MapCellType.NOTFREE)
                            {
                               image = ImageFactory.CreateTreeImage();
                               imageControl.Source = image;
                               imageControl.Width = 100;
                               imageControl.Height = 100;
                            }else{
                                if (game.gameContext.map.cells[i, j].cellType == MapCellType.ENEMY)
                                {
                                    image = ImageFactory.CreateEnemyImage();
                                }
                                else if (game.gameContext.map.cells[i, j].cellType == MapCellType.NEUTRAL)
                                {
                                    image = ImageFactory.CreateNeutralImage();
                                }
                                else
                                {
                                    image = ImageFactory.CreateChestImage();
                                }
                                imageControl.Source = image;
                                imageControl.Width = 70;
                                imageControl.Height = 70;
                            }
                            Canvas.SetTop(imageControl, i * 100);
                            Canvas.SetLeft(imageControl, j * 100);
                            mapCanvas.Children.Add(imageControl);
                            MapImages.Add(new Vector(i, j), imageControl);
                            if (game.gameContext.map.cells[i, j].cellType == MapCellType.ITEM)
                            {
                                Item currentItem = (Item)game.gameContext.map.cells[i, j].cellObject;
                                ToolTip toolTip = createToolTip(currentItem);
                                imageControl.ToolTip = toolTip;
                            }else if(game.gameContext.map.cells[i, j].cellType == MapCellType.NEUTRAL|| game.gameContext.map.cells[i, j].cellType == MapCellType.ENEMY)
                            {
                                Character currentUnit = (Character)game.gameContext.map.cells[i, j].cellObject;
                                ToolTip toolTip = createToolTip(currentUnit);
                                imageControl.ToolTip = toolTip;
                            }

                        }

                    
                    }
                }
            }
        }
        private ToolTip createToolTip(Character character)
        {
            ToolTip toolTip = new ToolTip();
            Grid grid = new Grid();
            RowDefinition[] rows = new RowDefinition[3];
            TextBlock[] texts = new TextBlock[3];
            for (int k = 0; k < 3; k++)
            {
                rows[k] = new RowDefinition();
                grid.RowDefinitions.Add(rows[k]);
                texts[k] = new TextBlock();
            }
            texts[1].Text = String.Format("HP: {0}/{0}", character.currentHP, character.maxHP);
            texts[2].Text = String.Format("MP: {0}/{0}", character.currentMP, character.maxMP);
            texts[0].Text = String.Format("Name: {0}", character.name);
            for (int k = 0; k < 3; k++)
            {
                Grid.SetRow(texts[k], k);
                grid.Children.Add(texts[k]);
            }
            toolTip.Content = grid;
            return toolTip;
        }
        private ContextMenu createMenuSmall()
        {
            ContextMenu menu = new ContextMenu();
            MenuItem takeOff = new MenuItem();
            takeOff.Header = "Take Off";
            //takeOff.Click=
            menu.Items.Add(takeOff);
            return menu;
        }
        private ContextMenu createMenu()
        {
            ContextMenu menu = new ContextMenu();
            MenuItem castOut = new MenuItem();
            castOut.Header = "CastOut";
            //castOut.Click =
            MenuItem wear = new MenuItem();
            wear.Header = "Wear";
            //wear.Click=
            menu.Items.Add(wear);
            menu.Items.Add(castOut);
            return menu;
        }
        private ToolTip createToolTip(Item item)
        {
            ToolTip toolTip = new ToolTip();
            
            Grid grid = new Grid();
            RowDefinition[] rows = new RowDefinition[12];
            TextBlock[] texts = new TextBlock[12];
            for (int k = 0; k < 12; k++)
            {
                rows[k] = new RowDefinition();
                grid.RowDefinitions.Add(rows[k]);
                texts[k] = new TextBlock();
            }
            texts[1].Text = String.Format("Strength: {0}", item.strength);
            texts[2].Text = String.Format("Perception: {0}", item.perception);
            texts[3].Text = String.Format("Endurance: {0}", item.endurance);
            texts[4].Text = String.Format("Charisma: {0}", item.charisma);
            texts[5].Text = String.Format("Intelligence: {0}", item.intelligence);
            texts[6].Text = String.Format("Agility: {0}", item.agility);
            texts[7].Text = String.Format("Luck: {0}", item.luck);
            texts[8].Text = String.Format("Damage: {0}", item.damage);
            texts[9].Text = String.Format("Armor: {0}", item.armor);
            texts[10].Text = String.Format("Price: {0}", item.price);
            texts[11].Text = String.Format("Size: {0}", item.size);
            texts[0].Text = String.Format("Name: {0}", item.name);
            for (int k = 0; k < 12; k++)
            {
                Grid.SetRow(texts[k], k);
                grid.Children.Add(texts[k]);
            }
            toolTip.Content = grid;
            return toolTip;
        }
        private void fight(object sender,EventArgs e)
        {
            //MapCell pos = character.playearState.position;
            //fightFacade.actionCharacter = (Character)pos.cellObject;
            bool win = game.Fight();
            if(PlayerCharacter.GetInstance().playearState.position.cellType==MapCellType.ENEMY)
            {
                if (win)
                {
                    Vector vector = new Vector(game.gameContext.player.position.positionX, game.gameContext.player.position.positionY);
                    Image img = MapImages[vector];
                    mapCanvas.Children.Remove(img);
                    MapImages.Remove(vector);
                }
                MessageBox.Show(String.Format("hp:{0}/{1}", game.gameContext.player.currentHealthPoints, game.gameContext.player.maxHealthPoints), win.ToString());
            }
            update();
            

        }
        private void pick(object sender, RoutedEventArgs e)
        {
            //MapCell pos = character.playearState.position;
            //fightFacade.item = (Item)pos.cellObject;
            //bool isPicked = fightFacade.pick();

            if (game.Pick())
            {
                Vector vector = new Vector(game.gameContext.player.position.positionX, game.gameContext.player.position.positionY);
                Image img = MapImages[vector];
                
                mapCanvas.Children.Remove(img);
                
                MapImages.Remove(vector);
                updateInventory();
                updateActiveSlots();
            }
            

        }
        private void updateInventory()
        {
            inventoryList.Items.Clear();
            foreach(Item item in game.gameContext.player.inventory.items)
            {
                ListBoxItem listItem = new ListBoxItem();
                listItem.Content = item.name;
                listItem.ToolTip = createToolTip(item);
                listItem.ContextMenu = createMenu();
                inventoryList.Items.Add(listItem);
            }
        }
        private void updateActiveSlots()
        {
            activeSlots.Children.Clear();
            var armor = PlayerCharacter.GetInstance().playearState.activeArmorSlots;
            List<Label> armorSlots = new List<Label>();
            for (int i = 0; i < armor.Length; i++)
            {
                armorSlots.Add(new Label ());
                if (armor[i] == null)
                {
                    armorSlots[i].Content = String.Format("Empty Armor Slot {0}",i+1);
                }
                else
                {
                    armorSlots[i].Content = armor[0].name;
                    armorSlots[i].ToolTip = createToolTip(armor[0]);
                    armorSlots[i].ContextMenu = createMenuSmall();
                }
                activeSlots.Children.Add(armorSlots[i]);
                Grid.SetRow(armorSlots[i], i);
            }
            var weapon = PlayerCharacter.GetInstance().playearState.activeWeaponSlot;
            Label weaponSlot = new Label();
            if (weapon == null)
            {
                weaponSlot.Content = "Empty Weapon Slot";
            }
            else
            {
                weaponSlot.Content = weapon.name;
                weaponSlot.ToolTip = createToolTip(weapon);
                weaponSlot.ContextMenu = createMenuSmall();
            }
            activeSlots.Children.Add(weaponSlot);
            Grid.SetRow(weaponSlot, armor.Length);
        }
        private void update()
        {
            showMap();
            showChar();
            updateInventory();
            updateStatus();
        
        }
        private void updateStatus()
        {
            double HorizontalOffset = scroll.HorizontalOffset;
            double VerticalOffset = scroll.VerticalOffset;

            if (mapCanvas.Children.Contains(currentState))
                mapCanvas.Children.Remove(currentState);
            currentState = new Label();
            switch (game.state)
            {
                case PausedState pause:
                    currentState.Content = "Павза";
                    currentState.FontSize = 50;
                    break;
                case HeroDeadState dead:
                    currentState.Content = "Смерть";
                    currentState.FontSize = 50;
                    break;
                default:
                    currentState.Content = "";
                    break;
            }
            Canvas.SetTop(currentState, VerticalOffset);
            Canvas.SetLeft(currentState, HorizontalOffset);
            mapCanvas.Children.Add(currentState);
        }
        private void KeyDownHandler(object sender, KeyEventArgs e)
        {
            double HorizontalOffset = scroll.HorizontalOffset;
            double VerticalOffset = scroll.VerticalOffset;
            if (e.Key == Key.F5)
            {
                try
                {
                    
                    careTaker.game = game;
                    careTaker.QuickSave();

                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
            else if (e.Key == Key.F9)
            {
                    if (e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
                    {

                        careTaker.QuickLoadPop();
                    }
                    else
                    {

                        careTaker.QuickLoad();
                    }

                    PlayerCharacter.GetInstance().playearState = game.gameContext.player;
                    //game.gameMode.context = game.gameContext;
                    update();
                
            }
            else
            {
                game.Move(e.Key);
                update();
            }
        }
        private void Save(object sender, EventArgs e)
        {
            SaveWindow window = new SaveWindow();
            window.Owner = this;
            if (window.ShowDialog() == true)
            {
                try
                {
                    
                    game.GameSave(window.result + ".dat", game.gameContext);
                }
                catch(Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        private void Pause(object sender, EventArgs e)
        {
            game.Pause();
            update();
        }

        private void Load(object sender, EventArgs e)
        {
            string currentDirectory = @"C:\Users\HP\Desktop\SE-15\мапз\lab6\Game\Game\bin\Debug";
            string[] saves = Directory.GetFiles(currentDirectory, "*.dat");
            Load window = new Load(saves);
            if (window.ShowDialog() == true)
            {
                game.GameLoad(window.result);
                PlayerCharacter.GetInstance().playearState = game.gameContext.player;
                //game.gameMode.context = game.gameContext;
                update();
            }

        }
        

    }
}
