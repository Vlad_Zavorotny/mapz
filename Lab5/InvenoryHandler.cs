﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameNameSpace
{
    abstract class ItemHandler
    {
        public ItemHandler next { get; private set; }
        public abstract bool Handle(Item item);
        public ItemHandler()
        {
            next = null;
        }
        public void SetNext(ItemHandler handler)
        {
            next = handler;
        }
    }

    class InvenoryHandler:ItemHandler
    {
        public override bool Handle(Item item)
        {
            int size=PlayerCharacter.GetInstance().playearState.inventory.occupiedPlace();
            if (size + item.size <= PlayerCharacter.GetInstance().playearState.inventory.size)
            {
                PlayerCharacter.GetInstance().playearState.inventory.add(item);
                if(next!=null)
                    next.Handle(item);
                return true;
            }
            return false;
        }
    }
    class ActiveArmorSlotHandler : ItemHandler {
        public override bool Handle(Item item)
        {
            if (item!=null&&item.itemType == ItemType.CLOTHING)
            {
                for (int i = 0; i < PlayerCharacter.GetInstance().playearState.activeArmorSlots.Length; i++)
                {

                    if (PlayerCharacter.GetInstance().playearState.activeArmorSlots[i] == null)
                    {
                        PlayerCharacter.GetInstance().playearState.activeArmorSlots[i] = (Clothing)item;
                        PlayerCharacter.GetInstance().playearState.inventory.delete(item);
                        item.modify();
                        return true;
                    }
                }
            }
            if(next!=null)
                next.Handle(item);
            return false;
        }
    }
    class ActiveWeaponSlotHandler : ItemHandler
    {
        public override bool Handle(Item item)
        {
            if(item!=null&&item.itemType== ItemType.WEAPON)
            {
                if (PlayerCharacter.GetInstance().playearState.activeWeaponSlot == null)
                {
                    PlayerCharacter.GetInstance().playearState.activeWeaponSlot = (Weapon)item;
                    PlayerCharacter.GetInstance().playearState.inventory.delete(item);
                    item.modify();
                    return true;
                }
            }
            if (next != null)
                next.Handle(item);
            return false;
        }
    }

}
