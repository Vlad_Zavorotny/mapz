﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameNameSpace
{
    interface IElement
    {
        void Accept(IVisitor visitor);
    }

    public interface IVisitor
    {
        void Visit(Map map);
        void Visit(MapCell mapCell);
        void Visit(Character character);
    }
    public class DifficultyIncresmentVisitor : IVisitor
    {
        public void Visit(Map map)
        {
            int additionalEnemies = 10;
            ICharacterFactory factory = new MediumCharacterFactory();
            Random rnd = new Random();
            for (int i = 0; i < additionalEnemies; i++)
            {
                int x = rnd.Next(0, map.maxX);
                int y = rnd.Next(0, map.maxY);
                if (map.cells[x,y].cellType == MapCellType.FREE)
                {
                    map.cells[x, y].cellObject = factory.GetEnemyCharacter();
                    map.cells[x, y].cellType = MapCellType.ENEMY;
                    ((Character)map.cells[x, y].cellObject).position = map.cells[x, y];
                }
            }
        }

        public void Visit(MapCell mapCell)
        {
            Random rnd = new Random();
            if (mapCell.cellType != MapCellType.END && mapCell.cellType != MapCellType.END)
            {
                if (rnd.Next(1, 250) < 5)
                {
                    ICharacterFactory factory = new StrongCharacterFactory();
                    mapCell.cellObject = factory.GetEnemyCharacter();
                    mapCell.cellType = MapCellType.ENEMY;
                    ((Character)mapCell.cellObject).position = mapCell;
                }
            }
        }

        public void Visit(Character character)
        {
            Random rnd = new Random();
            int attribute = rnd.Next(1, 4);
            character.strength += attribute;
            character.perception += attribute;
            character.endurance += attribute;
            character.charisma += attribute;
            character.agility += attribute;
            character.luck += attribute;
            character.intelligence += attribute;
            character.maxHP += attribute*3;
            character.currentHP = character.maxHP;
            character.maxMP += attribute * 3;
            character.currentMP = character.maxMP;
        }
    }
}
