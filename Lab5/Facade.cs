﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameNameSpace
{
    public class Facade
    {
        public PlayerCharacter character;
        public Item item { get; set; }
        public Character actionCharacter { get; set;}
        public Map map { get; set; }
        public Facade()
        {
            actionCharacter = null;
            item = null;
            character = PlayerCharacter.GetInstance();
        }
        public Facade(Map m,Character ch = null,Item it=null)
        {
            map = m;
            actionCharacter = ch;
            item = it;
            character = PlayerCharacter.GetInstance();
        }
        public bool fight()
        {
            if (PlayerCharacter.GetInstance().playearState.position.cellType != MapCellType.ENEMY)
                return true;
            int heroDamage = character.playearState.agility;
            if(character.playearState.activeWeaponSlot !=null)
                heroDamage += character.playearState.activeWeaponSlot.damage;
            int enemyDamage = actionCharacter.agility * 2;
            int heroStunProbability = character.playearState.strength;
            int enemyStunProbability = actionCharacter.strength*2;
            int heroArmor = 0;
            bool win = false;
            for(int i = 0; i < character.playearState.activeArmorSlots.Length; i++)
            {
                if(character.playearState.activeArmorSlots[i]!=null)
                    heroArmor += character.playearState.activeArmorSlots[i].armor;
            }
            int enemyArmor = 10;
            int heroAvoidence = character.playearState.endurance - actionCharacter.perception;
            int enemyAvoidence = actionCharacter.endurance - character.playearState.perception;
            int heroCritProbability = character.playearState.luck;
            int enemyCritProbability = actionCharacter.luck;
            double heroCritSize = 1 + (double)character.playearState.intelligence /10;
            double enemyCritSize = 1 + (double)actionCharacter.intelligence / 5;
            Random rand = new Random();
            bool heroIsStunned = false;
            bool enemyIsStunned = false;
            while (character.playearState.currentHealthPoints > 0 && actionCharacter.currentHP > 0)
            {
                if (heroIsStunned==false) {
                    if (enemyAvoidence+enemyArmor <= rand.Next(0, 100) || enemyIsStunned == true)
                    {
                        if (heroCritProbability > rand.Next(0, 100))
                        {
                            character.atack(actionCharacter, (int)(heroDamage * heroCritSize));
                        }
                        else
                        {
                            character.atack(actionCharacter, heroDamage);
                        }
                        if (heroStunProbability > rand.Next(0, 100))
                        {
                            enemyIsStunned = true;
                        }
                    }
                }
                else
                {
                    heroIsStunned = false;
                }
                if (actionCharacter.currentHP > 0)
                {
                    if (enemyIsStunned == false)
                    {
                        if (heroAvoidence+heroArmor<= rand.Next(0, 100) || heroIsStunned == true)
                        {
                            if (enemyStunProbability > rand.Next(0, 100))
                            {
                                actionCharacter.atack(character, (int)(enemyCritSize * enemyDamage));
                            }
                            else
                            {
                                actionCharacter.atack(character, enemyDamage);
                            }
                            if (enemyStunProbability > rand.Next(0, 100))
                            {
                                heroIsStunned = true;
                            }
                        }
                    }
                    else
                    {
                        enemyIsStunned = false;
                    }
                }
            }
            int goldReward = 0;
            int expReward = 0;
            if (character.playearState.currentHealthPoints > 0)
            {
                map.cleanCell(character.playearState.position);
                win = true;
                goldReward = actionCharacter.maxHP / 10;
                goldReward = goldReward + (int)((double)goldReward * (double)character.playearState.charisma / 10);
                expReward = actionCharacter.maxHP / 10;
                expReward = goldReward + (int)((double)goldReward * (double)character.playearState.charisma / 10);
                character.playearState.inventory.gold += goldReward;
                character.playearState.currentExp += expReward;
                while (character.playearState.currentExp > character.playearState.levelExp)
                {
                    character.levelUp();
                }
            }
            return win;
        }
        public bool pick()
        {
            if (PlayerCharacter.GetInstance().playearState.position.cellType != MapCellType.ITEM)
                return false;
            Item item = (Item)PlayerCharacter.GetInstance().playearState.position.cellObject;
            InvenoryHandler inventoryHandler = new InvenoryHandler();
            ActiveWeaponSlotHandler weaponHandler = new ActiveWeaponSlotHandler();
            ActiveArmorSlotHandler armorHandler = new ActiveArmorSlotHandler();
            armorHandler.SetNext(weaponHandler);
            inventoryHandler.SetNext(armorHandler);
            if (inventoryHandler.Handle(item))
            {
                map.cleanCell(character.playearState.position);
                return true;
            }
            return false;
        }
    }
}
