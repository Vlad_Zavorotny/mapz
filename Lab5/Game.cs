﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Windows.Input;

namespace GameNameSpace
{
    public class Game
    {
        public GameContext gameContext { get; set; }
        public int additionalDifficulty { get; set; }
        public GameMode gameMode { get;set; }
        public State state { get; set; }
        public Game(GameContext gameContext)
        {
            this.gameContext = gameContext;
            this.additionalDifficulty = 0;
            state = new InGameState(this);
            gameMode = new HardMode();
        }
        public Game()
        {
            gameContext = null;
            additionalDifficulty = 0;
            state = new InGameState(this);
            gameMode = new HardMode();
        }

        public bool Move(Key key)
        {
            return this.state.Move(key);
        }
        public bool Fight()
        {
            return this.state.Fight();
        }
        public bool Pick()
        {
            return this.state.Pick();
        }
        public GameMemento GameQuickSave()
        {
            //return new GameMemento((GameContext)this.gameContext.Clone());
            //try
            //{
                return this.state.GameQuickSave();
            //}
           // catch (Exception e){
            //    throw e;
           // }
        }
        public void MakeHarder()
        {
            DifficultyIncresmentVisitor visitor = new DifficultyIncresmentVisitor();
            for(int i = 0; i < additionalDifficulty; i++)
            {
                this.gameContext.map.Accept(visitor);
            }
        }
        public GameContext GameQuickLoad(GameMemento memento)
        {
            return this.state.GameQuickLoad(memento);
        }

        public void GameLoad(string loadName) {
            //BinaryFormatter formatter = new BinaryFormatter();
            //FileStream fs = new FileStream(loadName, FileMode.Open);
            //return (GameContext)formatter.Deserialize(fs);
            this.state.GameLoad(loadName);
        }

        public void GameSave(string saveName, GameContext state) {
            //BinaryFormatter formatter = new BinaryFormatter();
            //FileStream fs = new FileStream(saveName, FileMode.OpenOrCreate);
            //formatter.Serialize(fs, state);
            try
            {
                this.state.GameSave(saveName, state);
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public void Pause()
        {
            this.state.GamePause();
        }

    }
}
