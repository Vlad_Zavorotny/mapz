﻿using System;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
namespace GameNameSpace
{


    public enum MapCellType
    {
        FREE,
        NOTFREE,
        ENEMY,
        NEUTRAL,
        ITEM,
        START,
        END
    }
    [Serializable]
    public class MapCell:ICloneable,IElement
    {
        public int positionX { get; set; }
        public int positionY { get; set; }
        public bool isSet { get; set; }
        public MapCellType cellType { get; set; }
        public GameObject cellObject { get; set; }

        public MapCell() {
            positionX = 0;
            positionY = 0;
            cellType = MapCellType.FREE;
            cellObject = null;
            isSet = false;
        }

        public object Clone()
        {
            
            MapCell cell = new MapCell();
            if (cellObject != null)
            {
                cell.cellObject = (GameObject)cellObject.Clone();
            }
            else
            {
                cell.cellObject = null;
            }
            cell.positionX = positionX;
            cell.positionY = positionY;
            cell.isSet = isSet;
            cell.cellType = cellType;
            return cell;
        }

        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
            if (this.cellType == MapCellType.ENEMY)
            {
                ((Character)this.cellObject).Accept(visitor);
            }
        }
    }
    [Serializable]
    public class Map:ICloneable,IElement
    {
        public MapCell[,] cells { get; set; }
        public int maxX { get; set; }
        public int maxY { get; set; }
        public Map() {
            maxX = 0;
            maxY = 0;
            cells = null;
        }
        public void cleanCell(MapCell cell)
        {
            cell.cellObject = null;
            cell.cellType = MapCellType.FREE;
        }

        public object Clone()
        {
            Map map = new Map();
            map.maxX = this.maxX;
            map.maxY = this.maxY;
            map.cells = new MapCell[map.maxX, map.maxY];
            for(int i = 0; i < map.maxX; i++)
            {
                for(int j = 0; j < map.maxY; j++)
                {
                    map.cells[i, j] = (MapCell)this.cells[i, j].Clone();
                    if(map.cells[i,j].cellType==MapCellType.ENEMY|| map.cells[i, j].cellType == MapCellType.NEUTRAL)
                    {
                        ((Character)map.cells[i, j].cellObject).position = map.cells[i, j];
                    }
                }
            }
            return map;
        }

        public void Accept(IVisitor visitor)
        {
            visitor.Visit(this);
            for (int i = 0; i < this.maxX; i++)
            {
                for(int j = 0; j < this.maxY; j++)
                {
                    this.cells[i, j].Accept(visitor);
                }
            }
        }
    }
    public abstract class MapBuilder
    {
        protected Map map { get; private set; }
        public void CreateNewMap()
        {
            map = new Map();
        }
        public Map GetMyMap()
        {
            return map;
        }
        public abstract void SetSizeX();
        public abstract void SetSizeY();
        public abstract void Resize();
        public abstract void CreateCells();
        public abstract void SetStart();
        public abstract void SetEnd();
        public abstract void SetRoute();
        public abstract void SetCells();
        public abstract void SetPlayerCharacter();
        public abstract void SetNeutralCharacters();
        public abstract void SetEnemyCharacters();
        public abstract void SetItems();
    }
    public class TownMapBuilder : MapBuilder
    {
        public override void SetSizeX()
        {
            map.maxX = 40;
        }
        public override void SetSizeY()
        {
            map.maxY = 40;
        }
        public override void CreateCells()
        {
            for(int i = 0; i < this.map.maxX; i++)
            {
                for(int j = 0; j < this.map.maxY; j++)
                {
                    this.map.cells[i, j] = new MapCell();
                    this.map.cells[i, j].positionX = i;
                    this.map.cells[i, j].positionY = j;
                }
            }
        }
        public override void SetStart()
        {
            Random random = new Random();
            int startY = random.Next(0, this.map.maxY);
            map.cells[startY, 0].cellType = MapCellType.START;
            map.cells[startY, 0].isSet = true;
        }
        public override void Resize()
        {
            map.cells = new MapCell[map.maxX,map.maxY];     
        }
        public override void SetEnd()
        {
            Random random = new Random(1);
            int endY = random.Next(0, this.map.maxY);
            map.cells[endY, this.map.maxX - 1].cellType=MapCellType.END;
            map.cells[endY, this.map.maxX - 1].isSet = true;
        }
        public override void SetRoute()
        {
            int startY = -1;
            int endY = -1;
            for (int i = 0; i < this.map.maxY; i++)
            {
                if (this.map.cells[i, 0].cellType == MapCellType.START)
                    startY = i;
                if (this.map.cells[i, this.map.maxY - 1].cellType == MapCellType.END)
                    endY = i;
                if (startY >= 0 && endY >= 0)
                    break;
            }
            for (int i = 0; i < this.map.maxX - 1; i++)
            {
                this.map.cells[startY, i].isSet = true;
            }
            for (int i = Math.Min(startY, endY); i <= Math.Max(startY, endY); i++)
            {
                this.map.cells[i, this.map.maxX - 1].isSet = true;
            }
        }
        public override void SetCells() {
            int notFree = 0;
            int maxNotFree = 400;
            int posX;
            int posY;
            while (notFree <= maxNotFree)
            {
                Random random = new Random();
                posX = random.Next(0, this.map.maxX);
                posY = random.Next(0, this.map.maxY);
                if (this.map.cells[posX, posY].cellType == MapCellType.FREE&& this.map.cells[posX, posY].isSet==false)
                {
                    this.map.cells[posX, posY].cellType = MapCellType.NOTFREE;
                    notFree++;
                }
            }
        }
        public override void SetPlayerCharacter() {
            int startY = 0;

            for (int i = 0; i < this.map.maxY; i++)
            {
                if (this.map.cells[i, 0].cellType == MapCellType.START)
                    startY = i;
            }
            PlayerCharacter.GetInstance().playearState.position = this.map.cells[startY, 0];
        }
        public override void SetNeutralCharacters() {
            int neutrals = 0;
            int maxNeutrals = 10;
            int posX;
            int posY;
            int characterDifficulty;
            ICharacterFactory factory;
            Random random = new Random();
            while (neutrals <= maxNeutrals)
            {
                posX = random.Next(0, this.map.maxX);
                posY = random.Next(0, this.map.maxY);
                if (this.map.cells[posX, posY].cellType == MapCellType.FREE)
                {
                    characterDifficulty = random.Next(1, 11);
                    if (characterDifficulty < 3)
                        factory = new WeakCharacterFactory();
                    else if (characterDifficulty < 9)
                        factory = new MediumCharacterFactory();
                    else
                        factory = new StrongCharacterFactory();
                    this.map.cells[posX, posY].cellType = MapCellType.NEUTRAL;
                    NeutralCharacter neutral = factory.GetNeutralCharacter();
                    neutral.position = this.map.cells[posX, posY];
                    this.map.cells[posX, posY].cellObject = neutral;
                    neutrals++;
                }
            }
        }
        public override void SetEnemyCharacters()
        {
            int enemies = 0;
            int maxEnemies = 40;
            int posX;
            int posY;
            int characterDifficulty;
            ICharacterFactory factory;
            Random random = new Random();
            while (enemies <= maxEnemies)
            {
                posX = random.Next(0, this.map.maxX - 1);
                posY = random.Next(0, this.map.maxY - 1);
                if (this.map.cells[posX, posY].cellType == MapCellType.FREE)
                {
                    characterDifficulty = random.Next(1, 11);
                    if (characterDifficulty < 3)
                        factory = new WeakCharacterFactory();
                    else if (characterDifficulty < 9)
                        factory = new MediumCharacterFactory();
                    else
                        factory = new StrongCharacterFactory();
                    this.map.cells[posX, posY].cellType = MapCellType.ENEMY;
                    EnemyCharacter enemy = factory.GetEnemyCharacter();
                    enemy.position = this.map.cells[posX, posY];
                    this.map.cells[posX, posY].cellObject = enemy;
                    enemies++;
                }
            }
        }
    
        public override void SetItems() {
            {
   
                int items = 0;
                int maxEnemies = 40;
                int posX;
                int posY;
                int itemRarity;
                int itemType;
                IItemsFactory factory;
                Random random = new Random();
                while (items <= maxEnemies)
                {
                    posX = random.Next(0, this.map.maxX);
                    posY = random.Next(0, this.map.maxY);
                    if (this.map.cells[posX, posY].cellType == MapCellType.FREE)
                    {
                        itemRarity = random.Next(1, 347);
                        
                        itemType = random.Next(0,2);
                        if (itemRarity < 201)
                            factory = new StockItemsFactory();
                        else if (itemRarity < 301) 
                            factory = new CommonItemsFactory();
                        else if (itemRarity < 341) 
                            factory = new RareItemsFactory();
                        else if (itemRarity < 346)
                            factory = new EpicItemsFactory();
                        else
                            factory = new LegendaryItemsFactory();
                        this.map.cells[posX, posY].cellType = MapCellType.ITEM;
                        Item item;
                        if (itemType == 1)
                        item = factory.GetClothing();
                        else
                        item = factory.GetWeapon();
                        this.map.cells[posX, posY].cellObject = item;
                        items++;
                    }
                }
            }
        }
    }
    public class ForestMapBuilder:MapBuilder {
        public override void SetSizeX() {
            
        }

        public override void SetSizeY() { }
        public override void Resize() { }

        public override void CreateCells()
        {
            throw new NotImplementedException();
        }

        public override void SetStart() { }
        public override void SetEnd() { }
        public override void SetRoute() { }
        public override void SetCells() { }
        public override void SetPlayerCharacter() { }
        public override void SetNeutralCharacters() { }
        public override void SetEnemyCharacters() { }
        public override void SetItems() { }
    }
    public class DungeonMapBuilder:MapBuilder {
        public override void SetSizeX() { }
        public override void SetSizeY() { }
        public override void Resize() { }

        public override void CreateCells()
        {
            throw new NotImplementedException();
        }

        public override void SetStart() { }
        public override void SetEnd() { }
        public override void SetRoute() { }
        public override void SetCells() { }
        public override void SetPlayerCharacter() { }
        public override void SetNeutralCharacters() { }
        public override void SetEnemyCharacters() { }
        public override void SetItems() { }
    }
    class MapDirector {
        private MapBuilder mapBuilder;
        public void SetMapBuilder(MapBuilder _mapBuilder)
        {
            mapBuilder = _mapBuilder;
        }
        public Map GetMyMap()
        {
            return mapBuilder.GetMyMap();
        }
        public void ConstructMap()
        {
            mapBuilder.CreateNewMap();
            mapBuilder.SetSizeX();
            mapBuilder.SetSizeY();
            mapBuilder.Resize();
            mapBuilder.CreateCells();
            mapBuilder.SetStart();
            mapBuilder.SetEnd();
            mapBuilder.SetRoute();
            mapBuilder.SetCells();
            mapBuilder.SetPlayerCharacter();
            mapBuilder.SetNeutralCharacters();
            mapBuilder.SetEnemyCharacters();
            mapBuilder.SetItems();
        }
    }

}
