﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameNameSpace
{
    public abstract class MoveCommand
    {
        protected Character characterReceiver;
        protected Map map;
        public MoveCommand(Character receiver,Map map)
        {
            characterReceiver = receiver;
            this.map = map;
        }
        public abstract void Execute();
    }
    public class MoveLeft : MoveCommand
    {
        public MoveLeft(Character receiver, Map map) : base(receiver, map)
        {
        }

        public override void Execute()
        {
            characterReceiver.moveLeft(map);
        }
    }
    public class MoveRight : MoveCommand
    {
        public MoveRight(Character receiver, Map map) : base(receiver, map)
        {
        }

        public override void Execute()
        {
            characterReceiver.moveRight(map);
        }
    }
    public class MoveUp : MoveCommand
    {
        public MoveUp(Character receiver, Map map) : base(receiver, map)
        {
        }

        public override void Execute()
        {
            characterReceiver.moveUp(map);
        }
    }
    public class MoveDown : MoveCommand
    {
        public MoveDown(Character receiver, Map map) : base(receiver, map)
        {
        }

        public override void Execute()
        {
            characterReceiver.moveDown(map);
        }
    }
    class MoveInvoker
    {
        private List<MoveCommand> commands { get; set; }
        public MoveInvoker()
        {
            commands = new List<MoveCommand>();
        }
        public void AddCommand(MoveCommand command)
        {
            commands.Add(command);
        }
        public void executeCommands()
        {
            while (commands.Count > 0)
            {
                commands.ElementAt(0).Execute();
                commands.RemoveAt(0);
            }
        }
    }
}
