﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameNameSpace
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void StartGameButtonClick(object sender, RoutedEventArgs e) {
            GameWindow window = new GameWindow();
            window.Show();
        }
        private void LoadGameButtonClick(object sender, RoutedEventArgs e) { }
        private void EndGameButtonClick(object sender, RoutedEventArgs e) {
            this.Close();
        }
        private void SettingsButtonClick(object sender, RoutedEventArgs e) { }
        
    }
}
