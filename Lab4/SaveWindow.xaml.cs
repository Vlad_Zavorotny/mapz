﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameNameSpace
{
    /// <summary>
    /// Interaction logic for SaveWindow.xaml
    /// </summary>
    public partial class SaveWindow : Window
    {
        public string result { get; set; }
        public SaveWindow()
        {
            InitializeComponent();
            
        }
        public void Save(object sender, EventArgs e)
        {
            result = textBox.Text;
            this.DialogResult = true;
        }
    }
}
