﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameNameSpace
{
    public abstract class GameMode
    {
       // public GameContext context { get; set; }
        public abstract bool MakeTurn(GameContext context);
    
    }
    public class EasyMode : GameMode
    {
        public EasyMode()
        {
            //this.context = context;
        }
        public override bool MakeTurn(GameContext context)
        {
            return false;
        }
    }
    public class NormalMode : GameMode
    {
        public NormalMode()
        {
           // this.context = context;
        }
        public override bool MakeTurn(GameContext context)
        {
            if (context.player.position.cellType == MapCellType.ENEMY)
            {
                return true;
            }
            return false;
        }
    }
    public class HardMode : GameMode
    {
        private MoveInvoker moves;
        public HardMode()
        {
            //this.context = context;
            moves = new MoveInvoker();
        }
        private List<EnemyCharacter> GetEnemies(int agreRange, GameContext context)
        {
            List<EnemyCharacter> enemies = new List<EnemyCharacter>();
            int minX, maxX, minY, maxY;
            minX = Math.Max(0, context.player.position.positionX - agreRange);
            minY = Math.Max(0, context.player.position.positionY - agreRange);
            maxX = Math.Min(context.map.maxX-1, context.player.position.positionX + agreRange);
            maxY = Math.Min(context.map.maxX - 1, context.player.position.positionY + agreRange);
            for(int i = minX; i <= maxX; i++)
            {
                for(int j = minY; j <= maxY; j++)
                {
                    if (context.map.cells[i, j].cellType == MapCellType.ENEMY)
                        enemies.Add((EnemyCharacter)context.map.cells[i, j].cellObject);
                }
            
            }
            return enemies;
        }
        private void Move(EnemyCharacter enemy, GameContext context)
        {
            int addedCommands = 0;
            //UP
            if (enemy.position.positionX > context.player.position.positionX)
            {
                if (enemy.position.positionX > 0 && context.map.cells[enemy.position.positionX - 1, enemy.position.positionY].cellType == MapCellType.FREE)
                {
                    if (addedCommands == 0)
                    {
                        moves.AddCommand(new MoveUp(enemy, context.map));
                        addedCommands++;
                    }
                    
                }
            //DOWN
            }else if(enemy.position.positionX < context.player.position.positionX)
            {
                if (enemy.position.positionX < context.map.maxX - 1 && context.map.cells[enemy.position.positionX + 1, enemy.position.positionY].cellType == MapCellType.FREE)
                {

                    if (addedCommands == 0)
                    {
                        moves.AddCommand(new MoveDown(enemy, context.map));
                        addedCommands++;
                    }
                }
            }
            //LEFT
            if(enemy.position.positionY > context.player.position.positionY)
            {
                if (enemy.position.positionY > 0 && context.map.cells[enemy.position.positionX, enemy.position.positionY - 1].cellType == MapCellType.FREE)
                {
                    if (addedCommands == 0)
                    {
                        moves.AddCommand(new MoveLeft(enemy, context.map));
                        addedCommands++;
                    }
                }
            //RIGHT
            }else if (enemy.position.positionY < context.player.position.positionY)
            {
                if (enemy.position.positionY < context.map.maxY - 1 && context.map.cells[enemy.position.positionX, enemy.position.positionY + 1].cellType == MapCellType.FREE)
                {
                    if (addedCommands == 0)
                    {
                        moves.AddCommand(new MoveRight(enemy, context.map));
                        addedCommands++;
                    }
                }
            }
        }
        public override bool MakeTurn(GameContext context)
        {
            int agreRandge = 3;
            List<EnemyCharacter> enemies = GetEnemies(agreRandge,context);
            foreach(EnemyCharacter enemy in enemies)
            {
                Move(enemy,context);
                moves.executeCommands();
            }
            
            if (context.player.position.cellType == MapCellType.ENEMY)
            {
                return true;
            }
            return false;
        }
    }
}
