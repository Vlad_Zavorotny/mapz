﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Controls;

namespace GameNameSpace
{
        class ImageFactory
        {
            private static Dictionary<Type, BitmapImage> Images = new Dictionary<Type, BitmapImage>();
            
            public static BitmapImage CreateChestImage()
            {
                if (!Images.ContainsKey(typeof(Item)))
                {
                    BitmapImage image;
                    string path = "resource/chest.png";
                    image = new BitmapImage(new Uri(path, UriKind.Relative));
                    Images.Add(typeof(Item), image);
                }
                return Images[typeof(Item)];
            }
            public static BitmapImage CreateHeroImage()
            {
                if (!Images.ContainsKey(typeof(PlayerCharacter)))
                {
                    BitmapImage image;
                    string path = "resource/character.png";
                    image = new BitmapImage(new Uri(path, UriKind.Relative));
                    Images.Add(typeof(PlayerCharacter), image);
                }
                return Images[typeof(PlayerCharacter)];
            }
            public static BitmapImage CreateEnemyImage()
            {
                if (!Images.ContainsKey(typeof(EnemyCharacter)))
                {
                    BitmapImage image;
                    string path = "resource/enemy.png";
                    image = new BitmapImage(new Uri(path, UriKind.Relative));
                    Images.Add(typeof(EnemyCharacter), image);
                }
                return Images[typeof(EnemyCharacter)];

            }
            public static BitmapImage CreateTreeImage()
            {
                if (!Images.ContainsKey(typeof(int)))
                {
                    BitmapImage image;
                    string path = "resource/tree.png";
                    image = new BitmapImage(new Uri(path, UriKind.Relative));
                    Images.Add(typeof(int), image);
                }
                return Images[typeof(int)];
            }
            public static BitmapImage CreateNeutralImage() 
            {
                if (!Images.ContainsKey(typeof(NeutralCharacter)))
                {
                    BitmapImage image;
                    string path = "resource/npc.png";
                    image = new BitmapImage(new Uri(path, UriKind.Relative));
                    Images.Add(typeof(NeutralCharacter), image);
                }
                return Images[typeof(NeutralCharacter)];
            } 
        }

    }
