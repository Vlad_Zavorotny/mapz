﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameNameSpace
{
    [Serializable]
    public class Inventory:ICloneable
    {
        public int size { get; set; }
        public int gold { get; set; }
        public List<Item> items { get; set; }
        public Inventory()
        {
            size = 100;
            gold = 0;
            items = new List<Item>();
        }
        public int occupiedPlace()
        {
            int ocSize = 0;
            foreach (Item item in items)
            {
                ocSize += item.size;
            }
            return ocSize;
        }
        public void add(Item item) {
            items.Add(item);
        }
        public void delete(Item item) {
            items.Remove(item);
        }

        public object Clone()
        {
            Inventory inventory = new Inventory();
            inventory.gold = gold;
            inventory.size = this.size;
            inventory.items = new List<Item>();
            for(int i = 0; i < items.Count; i++)
            {
                inventory.items.Add((Item)items[i].Clone());
            }
            return inventory;
        }
    }
    public enum ItemType
    {
        CLOTHING,
        WEAPON
    }


    [Serializable]
    public abstract class  Item : GameObject {
        public int price { get; set; }
        public int size { get; set; }
        public int armor { get; set; }
        public int damage { get; set; }
        public ItemType itemType { get; set; }
        protected int calculatePrice()
        {
            double initPrice = 100;
            double price = 0;
            double modifiers = 0;
            modifiers += strength - 1;
            modifiers += perception - 1;
            modifiers += endurance - 1;
            modifiers += charisma - 1;
            modifiers += intelligence - 1;
            modifiers += agility - 1;
            modifiers += luck - 1;
            modifiers += armor - 5;
            modifiers += 7 - size;
            modifiers += damage / 2;
            modifiers /= 10;
            
            price = initPrice + initPrice * modifiers;
            if (price < 0)
                return 0;
            return (int)price;
        }
        public Item(){
            price = 0;
            size = 0;
            armor = 0;
            damage = 0;
            image = ImageFactory.CreateChestImage();
        }
        public void modify()
        {
            PlayerCharacter.GetInstance().playearState.strength += this.strength;
            PlayerCharacter.GetInstance().playearState.perception += this.perception;
            PlayerCharacter.GetInstance().playearState.endurance += this.endurance;
            PlayerCharacter.GetInstance().playearState.charisma += this.charisma;
            PlayerCharacter.GetInstance().playearState.intelligence += this.intelligence;
            PlayerCharacter.GetInstance().playearState.agility += this.agility;
            PlayerCharacter.GetInstance().playearState.luck += this.luck;
        }
        public void demodify()
        {
            PlayerCharacter.GetInstance().playearState.strength -= this.strength;
            PlayerCharacter.GetInstance().playearState.perception -= this.perception;
            PlayerCharacter.GetInstance().playearState.endurance -= this.endurance;
            PlayerCharacter.GetInstance().playearState.charisma -= this.charisma;
            PlayerCharacter.GetInstance().playearState.intelligence -= this.intelligence;
            PlayerCharacter.GetInstance().playearState.agility -= this.agility;
            PlayerCharacter.GetInstance().playearState.luck -= this.luck;
        }

        public override void atack(PlayerCharacter character, int damage = 0)
        {

        }

    }
    [Serializable]
    public abstract class Clothing : Item {
        public Clothing(){
            itemType = ItemType.CLOTHING;
        }
    }
    [Serializable]
    public abstract class Weapon:Item
    {
        public Weapon() {
            itemType = ItemType.WEAPON;
        }
    }
    [Serializable]
    public class StockClothing:Clothing {
        public StockClothing()
        {
            name = "Stock Clothing";
            Random random=new Random();
            strength = random.Next(0, 2);
            perception = random.Next(0, 2);
            endurance = random.Next(0, 2);
            charisma = random.Next(0, 2);
            intelligence = random.Next(0, 2);
            agility = random.Next(0, 2);
            luck = random.Next(0, 2);
            size = random.Next(4, 10);
            armor = random.Next(0, 10);
            price = calculatePrice();
        }
    }
    [Serializable]
    public class StockWeapon : Weapon {
        public StockWeapon()
        {
            name = "Stock Weapon";
            Random random = new Random();
            strength = random.Next(0, 2);
            perception = random.Next(0, 2);
            endurance = random.Next(0, 2);
            charisma = random.Next(0, 2);
            intelligence = random.Next(0, 2);
            agility = random.Next(0, 2);
            luck = random.Next(0, 2);
            size = random.Next(4, 10);
            damage = random.Next(0, 10);
            price = calculatePrice();
        }
    }
    [Serializable]
    public class CommonClothing : Clothing {
        public CommonClothing()
        {
            name = "Common Clothing";
            Random random = new Random();
            strength = random.Next(1, 4);
            perception = random.Next(1, 4);
            endurance = random.Next(1, 4);
            charisma = random.Next(1, 4);
            intelligence = random.Next(1, 4);
            agility = random.Next(1, 4);
            luck = random.Next(1, 4);
            size = random.Next(4, 10);
            armor = random.Next(5, 16);
            price = calculatePrice();
        }
    }
    [Serializable]
    public class CommonWeapon: Weapon {
        public CommonWeapon()
        {
            name = "Common Weapon";
            Random random = new Random();
            strength = random.Next(1, 4);
            perception = random.Next(1, 4);
            endurance = random.Next(1, 4);
            charisma = random.Next(1, 4);
            intelligence = random.Next(1, 4);
            agility = random.Next(1, 4);
            luck = random.Next(1, 4);
            size = random.Next(4, 10);
            damage = random.Next(5, 16);
            price = calculatePrice();
        }
    }
    [Serializable]
    public class RareClothing : Clothing {
        public RareClothing()
        {
            name = "Rare Clothing";
            Random random = new Random();
            strength = random.Next(3, 6);
            perception = random.Next(3, 6);
            endurance = random.Next(3, 6);
            charisma = random.Next(3, 6);
            intelligence = random.Next(3, 6);
            agility = random.Next(3, 6);
            luck = random.Next(3, 6);
            size = random.Next(4, 10);
            armor = random.Next(12, 24);
            price = calculatePrice();
        }
    }
    [Serializable]
    public class RareWeapon : Weapon {
        public  RareWeapon()
        {
            name = "Rare Weapon";
            Random random = new Random();
            strength = random.Next(3, 6);
            perception = random.Next(3, 6);
            endurance = random.Next(3, 6);
            charisma = random.Next(3, 6);
            intelligence = random.Next(3, 6);
            agility = random.Next(3, 6);
            luck = random.Next(3, 6);
            size = random.Next(4, 10);
            damage = random.Next(12, 24);
            price = calculatePrice();
        }
    }
    [Serializable]
    public class EpicClothing : Clothing {
        public EpicClothing()
        {
            name = "Epic Clothing";
            Random random = new Random();
            strength = random.Next(5, 9);
            perception = random.Next(5, 9);
            endurance = random.Next(5, 9);
            charisma = random.Next(5, 9);
            intelligence = random.Next(5, 9);
            agility = random.Next(5, 9);
            luck = random.Next(5, 9);
            size = random.Next(4, 10);
            armor = random.Next(20, 32);
            price = calculatePrice();
        }
    }
    [Serializable]
    public class EpicWeapon: Weapon {
        public EpicWeapon()
        {
            name = "Epic Weapon";
            Random random = new Random();
            strength = random.Next(5, 9);
            perception = random.Next(5, 9);
            endurance = random.Next(5, 9);
            charisma = random.Next(5, 9);
            intelligence = random.Next(5, 9);
            agility = random.Next(5, 9);
            luck = random.Next(5, 9);
            size = random.Next(4, 10);
            damage = random.Next(20, 32);
            price = calculatePrice();
        }
    }
    [Serializable]
    public class LegendaryClothing : Clothing {
        public LegendaryClothing()
        {
            name = "Legendary Clothing";
            Random random = new Random();
            strength = random.Next(7, 12);
            perception = random.Next(7, 12);
            endurance = random.Next(7, 12);
            charisma = random.Next(7, 12);
            intelligence = random.Next(7, 12);
            agility = random.Next(7, 12);
            luck = random.Next(7, 12);
            size = random.Next(4, 10);
            armor = random.Next(28, 56);
            price = calculatePrice();
        }
    }
    [Serializable]
    public class LegendaryWeapon : Weapon {
        public LegendaryWeapon()
        {
            name = "Legendary Weapon";
            Random random = new Random();
            strength = random.Next(7, 12);
            perception = random.Next(7, 12);
            endurance = random.Next(7, 12);
            charisma = random.Next(7, 12);
            intelligence = random.Next(7, 12);
            agility = random.Next(7, 12);
            luck = random.Next(7, 12);
            size = random.Next(4, 10);
            armor = random.Next(28, 56);
            price = calculatePrice();
        }
    }
    public interface IItemsFactory {
        Clothing GetClothing();
        Weapon GetWeapon();
    }
    public class StockItemsFactory : IItemsFactory {
        public Clothing GetClothing()
        {
            return new StockClothing();
        }
        public Weapon GetWeapon()
        {
            return new StockWeapon();
        }
    }
    public class CommonItemsFactory : IItemsFactory
    {
        public Clothing GetClothing()
        {
            return new CommonClothing();
        }
        public Weapon GetWeapon()
        {
            return new CommonWeapon();
        }
    }
    public class RareItemsFactory : IItemsFactory
    {
        public Clothing GetClothing()
        {
            return new RareClothing();
        }
        public Weapon GetWeapon()
        {
            return new RareWeapon();
        }
    }
    public class EpicItemsFactory : IItemsFactory
    {
        public Clothing GetClothing()
        {
            return new EpicClothing();
        }
        public Weapon GetWeapon()
        {
            return new EpicWeapon();
        }
    }
    public class LegendaryItemsFactory : IItemsFactory
    {
        public Clothing GetClothing()
        {
            return new LegendaryClothing();
        }
        public Weapon GetWeapon()
        {
            return new LegendaryWeapon();
        }
    }


}
