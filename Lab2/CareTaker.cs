﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameNameSpace
{
    public class CareTaker
    {
        private readonly Stack<GameMemento> quickSaves = new Stack<GameMemento>();
        public Game game { get; set; }
        public CareTaker(Game game)
        {
            this.game = game;
        }
        public void QuickSave()
        {
            quickSaves.Push(game.GameQuickSave());
        }
        public void QuickLoad()
        {
            if (quickSaves.Count > 0)
            {
                game.gameContext = (GameContext)game.GameQuickLoad(quickSaves.Peek()).Clone();
                //game.gameMode.context = game.gameContext;
            }
        }
        public void QuickLoadPop()
        {
            if (quickSaves.Count > 0)
            {
                game.gameContext = (GameContext)game.GameQuickLoad(quickSaves.Pop()).Clone();
                //game.gameMode.context = game.gameContext;
            }
        }
    }
}
