﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Input;

namespace GameNameSpace
{
    public abstract class State
    {
        protected Game game;
        public State(Game game)
        {
            this.game = game;
        }
        public abstract void GamePause();
        public abstract void GameLoad(string loadName);
        public abstract void GameSave(string saveName, GameContext state);
        public abstract GameContext GameQuickLoad(GameMemento memento);
        public abstract GameMemento GameQuickSave();
        public abstract bool Move(Key key);
        public abstract bool Fight();
        public abstract bool Pick();
    }
    public class InGameState : State
    {
        public InGameState(Game game) : base(game)
        {
        }

        public override bool Fight()
        {
            Facade fightFacade = new Facade();
            fightFacade.map = game.gameContext.map;
            PlayerCharacter character = PlayerCharacter.GetInstance();
            fightFacade.character = character;
            MapCell pos = character.playearState.position;
            fightFacade.actionCharacter = (Character)pos.cellObject;
            bool win = fightFacade.fight();
            if (win == false)
            {
                game.state = new HeroDeadState(game);
            }
            return win;
        }

        public override void GameLoad(string loadName)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream(loadName, FileMode.Open);
            game.gameContext=(GameContext)formatter.Deserialize(fs);
            //game.gameMode.context = game.gameContext;
        }

        public override void GamePause()
        {
            game.state = new PausedState(game);    
        }

        public override GameContext GameQuickLoad(GameMemento memento)
        {
            return memento.GetState();
        }

        public override GameMemento GameQuickSave()
        {
            GameContext context = (GameContext)game.gameContext.Clone();
            return new GameMemento(context);
        }

        public override void GameSave(string saveName, GameContext state)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream(saveName, FileMode.OpenOrCreate);
            formatter.Serialize(fs, state);
        }

        public override bool Move(Key key)
        {
            bool canMove;
            if (key == Key.W)
            {
                canMove=PlayerCharacter.GetInstance().moveUp(game.gameContext.map);

            }
            else if (key == Key.S)
            {
                canMove=PlayerCharacter.GetInstance().moveDown(game.gameContext.map);
            }
            else if (key == Key.A)
            {
                canMove = PlayerCharacter.GetInstance().moveLeft(game.gameContext.map);
            }
            else if (key == Key.D)
            {
                canMove = PlayerCharacter.GetInstance().moveRight(game.gameContext.map);
            }
            else
            {
                canMove =false;
            }
            this.game.gameContext.player = PlayerCharacter.GetInstance().playearState;
            bool fightRequired = this.game.gameMode.MakeTurn(game.gameContext);
            if (fightRequired == true)
            {
                this.Fight();
            }
            return canMove;
        }

        public override bool Pick()
        {
            Facade fightFacade = new Facade();
            fightFacade.map = game.gameContext.map;
            PlayerCharacter character = PlayerCharacter.GetInstance();
            MapCell pos = character.playearState.position;
            fightFacade.item = (Item)pos.cellObject;
            bool isPicked = fightFacade.pick();
            return isPicked;
        }
    }
    public class PausedState : State
    {
        public PausedState(Game game) : base(game)
        {
        }

        public override bool Fight()
        {
            return true;
        }

        public override void GameLoad(string loadName)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream(loadName, FileMode.Open);
            game.gameContext = (GameContext)formatter.Deserialize(fs);
            //game.gameMode.context = game.gameContext;
        }

        public override void GamePause()
        {
            game.state = new InGameState(game);
        }

        public override GameContext GameQuickLoad(GameMemento memento)
        {
            return memento.GetState();
        }

        public override GameMemento GameQuickSave()
        {
            return new GameMemento((GameContext)game.gameContext.Clone());
        }

        public override void GameSave(string saveName, GameContext state)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream(saveName, FileMode.OpenOrCreate);
            formatter.Serialize(fs, state);
        }

        public override bool Move(Key key)
        {
            return false;
        }

        public override bool Pick()
        {
            return false;
        }
    }
    public class HeroDeadState : State
    {
        public HeroDeadState(Game game) : base(game)
        {
        }

        public override bool Fight()
        {
            return false;
        }

        public override void GameLoad(string loadName)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream(loadName, FileMode.Open);
            game.gameContext = (GameContext)formatter.Deserialize(fs);
            //game.gameMode.context = game.gameContext;
            game.state = new InGameState(game);
        }

        public override void GamePause()
        {
            
        }

        public override GameContext GameQuickLoad(GameMemento memento)
        {
            game.state = new InGameState(game);
            return memento.GetState();
        }

        public override GameMemento GameQuickSave()
        {
            throw new Exception("Cannot Save Game with dead playear character");  
        }

        public override void GameSave(string saveName, GameContext state)
        {
            throw new Exception("Cannot Save Game with dead playear character");
        }

        public override bool Move(Key key)
        {
            return false;
        }

        public override bool Pick()
        {
            return false;
        }
    }

}
