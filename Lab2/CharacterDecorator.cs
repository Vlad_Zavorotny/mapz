﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameNameSpace
{
    public abstract class CharacterDecorator:BaseObject
    {
        protected BaseObject wrappee;
        public CharacterDecorator(BaseObject character)
        {
            wrappee = character;
        }
        public void SetCharacter(BaseObject character)
        {
            wrappee = character;
        }
        public override void atack(PlayerCharacter character, int damage = 0)
        {
            if (wrappee != null)
            {
                wrappee.atack(character, damage);
            }
        }
    }
    public class DoomedCharacterDecorator : CharacterDecorator {
        DoomedCharacterDecorator(BaseObject character) : base(character) { }
        public override void atack(PlayerCharacter character, int damage = 0)
        {
            base.atack(character, damage);
            if (character.playearState.maxHealthPoints > 0)
            {
                character.playearState.maxHealthPoints--;
                if (character.playearState.maxHealthPoints < character.playearState.currentHealthPoints)
                {
                    character.playearState.currentHealthPoints = character.playearState.maxHealthPoints;
                }
            }
        }
    }
    public class ToxicCharacterDecorator : CharacterDecorator {
        public ToxicCharacterDecorator(BaseObject character):base(character) { }

        public override void atack(PlayerCharacter character, int damage = 0)
        {
            base.atack(character, damage);
            character.playearState.currentHealthPoints--;
        }
    }
    public class GreedyCharacterDecorator : CharacterDecorator {
        public GreedyCharacterDecorator(BaseObject character) : base(character) { }

        public override void atack(PlayerCharacter character, int damage = 0)
        {
            base.atack(character, damage);
            character.playearState.inventory.gold -= 2;
        }
    }
    public class DamagedCharacterDecorator:CharacterDecorator
    {
        public DamagedCharacterDecorator(BaseObject character): base(character) { }

        public override void atack(PlayerCharacter character, int damage = 0)
        {
            base.atack(character, damage);
            Random rnd = new Random();
            if (rnd.Next(0, 10) > 7)
            {
                if (character.playearState.activeWeaponSlot != null && character.playearState.activeWeaponSlot.damage > 0)
                {
                    character.playearState.activeWeaponSlot.damage--;
                }
            }
        }
    }


}
