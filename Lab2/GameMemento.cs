﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameNameSpace
{
    public class GameMemento
    {
        private readonly GameContext _state;
        public GameMemento(GameContext state)
        {
            _state = state;
        }
        public GameContext GetState()
        {
            return _state;
        }
    }
}
