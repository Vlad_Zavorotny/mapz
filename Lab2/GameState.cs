﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameNameSpace
{
    [Serializable]
    public class GameContext:ICloneable
    {
        public Map map { get; set; }
        public PlayearCharacterState player { get; set; }
        public GameContext(Map map, PlayearCharacterState player)
        {
            this.map = map;
            this.player = (PlayearCharacterState)player.Clone();
        }
        public GameContext()
        {
            map = null;
            player = null;
        }

        public object Clone()
        {
            GameContext state=new GameContext();
            state.map = (Map)this.map.Clone();
            state.player = (PlayearCharacterState)this.player.Clone();
            return state;

        }
    }
}
